<?php
/**
 * Template Name: Blog
 */
get_header();

?>

<?php

    $exlude = get_category_by_slug('guides');
    $exlude_id = $exlude->term_id;

    $args = array(
        'post_type'   => 'post',
        'category__not_in' => array($exlude_id),
        'posts_per_page' => '10',
        'orderby' => 'modified', 
        'order' => 'DESC',
        );

    $latest_posts = new WP_query ( $args );
    $flag = 1;
    if ( $latest_posts->have_posts() ) {
    while ( $latest_posts->have_posts() ) {
            $latest_posts->the_post(); 
    ?>
    
    <section>
        <div class="container-post__wrapper container-fluid fade">
            <div class="container-post /*container-page*/ p-0">

            <?php if($flag) { ?>
                
                <div class="post-header__container pb-0 pt-3">
                    <div class="section-header p-4">
                    <img src="<?php echo get_template_directory_uri();?>/assets/icons/blog.svg" width="50" height="50">
                    <h1>Блог</h1>
                    </div>
                </div>
                           
                <?php $flag = 0;
                } ?>

                <?php if (!empty(wp_get_post_terms(get_the_ID(),'location'))) {                                      
                    echo '<div class="breadcrumbs__container-blog container-fluid">';
                    post_location_breadcrumbs();
                    echo '</div>';
                } ?>   

                <div class="container-post__inner container-blog__inner">
                    <a href="<?php echo get_the_permalink(get_the_ID());?>">
                    <div class="post-featured-img__wrapper"> 
                        <div class="post-featured-img__inner"><?php the_post_thumbnail('large'); ?></div>
                    </div>
                    <div class="post-header__container py-3">
                        <div class="post-header__wrapper">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                    </a>
                    <?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?>
                    <div class="read-more">
                        <p><a href="<?php echo get_the_permalink($related_posts__id);?>">Читать полностью »</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php } 
      ?>

        <div id="posts" data-max-page="<?php echo $latest_posts->max_num_pages;?>" data-page-type="blog"></div>

        
<?php    } 
    if ( $latest_posts->max_num_pages > 1 ) { ?>
        <div class="section-button">
            <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
            <div id="loader" style="display:none; margin:10px 0;"></div>
        </div>

    <?php } 
?>

<?php
    wp_reset_postdata(); ?>

<?php 
get_footer(); 
?>