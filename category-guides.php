<?php
/**
 * Template Name: Категория Все путеводители
 */
get_header();
?>

<?php

    // Guides

    $cities[] = array(
        'post_id' => get_the_ID(),
        'term_id' => $post_deep_term->term_id,
    );

    $guide_ids = return_guide_ids();
    $guide_names = return_guide_names();
    $guide_names[] = sort($guide_names, SORT_LOCALE_STRING );

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'guides',
        'orderby' => 'name', 
        'posts_per_page' => '-1',
        'tax_query' => array(
            array(
                'taxonomy' => 'location',
                'field'    => 'term_id',
                'terms'    => $guide_ids,
                ),
        ),
    );

    $guides = new WP_query ( $args );
    $post_ids = wp_list_pluck( $guides->posts, 'ID' );
        
    ?>

    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/guide.svg" width="50" height="50">
            <h1>Все путеводители</h1>
            </div>
            <div class="section-content fade">
                <div class="container">
                <div class="card-container p-0">       
                   
                <?php
                if(!empty($post_ids)) {
                foreach ($post_ids as $post_id) {
                    
                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);

                        foreach ($deep_terms as $deep_term) {
                             $sorted_guides[] = array(

                                'post_id' => $post_id,
                                'term_id' => $deep_term->term_id,
                                'term_name' => $deep_term->name,
                                'term_type' => get_term_meta( $deep_term->term_id, 'location-type', true ),

                                 );

                                $keys = array_column($sorted_guides, 'term_name');
                                array_multisort($keys,  SORT_LOCALE_STRING , $sorted_guides);
                        }
                } }
                    
                    wp_reset_postdata();  
                    
                    if(!empty($sorted_guides)) {
                    foreach ($sorted_guides as $sorted_guide) {
                        
                    $check_type = $sorted_guide['term_type'];
                       if ($check_type == 'city') {
                       
                            $parent_id = wp_get_term_taxonomy_parent_id($sorted_guide['term_id'], 'location');
                            $guide_flag = get_term_meta( $parent_id, 'flag-url', true );
                            $guide_title = $sorted_guide['term_name'];
                            $guide_url = get_term_link($sorted_guide['term_id']);
                            $guide_img = get_the_post_thumbnail_url( $sorted_guide['post_id'] , 'medium_large' );

                        }
                        else if ($check_type == 'country') {
                                $guide_flag = get_term_meta( $sorted_guide['term_id'], 'flag-url', true );
                                $guide_title = $sorted_guide['term_name'];
                                $guide_url = get_term_link($sorted_guide['term_id']);
                                $guide_img = get_the_post_thumbnail_url( $sorted_guide['post_id'] , 'medium_large' );
                        } ?>
                          
                            <div class="card-wrapper-main card-wrapper-page m-0 fade">
                                <div class="card-wrapper">
                                    <a href="<?php echo $guide_url;?>">
                                        <div class="card-image" style="background-image:url('<?php echo $guide_img; ?>')">
                                        <div class="card-flag text-right"><img src="<?php echo $guide_flag; ?>" width="35" height="25"></div>
                                        <div class="card-title"><?php echo $guide_title; ?></div>

                                        <div class="card-category-wrapper">

                                        <div class="card-icon">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/icons/location.svg" width="13" height="21">
                                        </div>

                                        <div class="card-category">Путеводитель</div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            
                        <?php  } } ?>
        
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
?>