<?php
/**
 * Template Name: Категории
 */
get_header();
?>
    <?php

    $args = array(
        'post_type'   => 'post',
        'category__in' => get_query_var('cat'),
        'posts_per_page' => '6',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => 1,
        );

    $category_posts = new WP_query ( $args );
    ?>

    <section class="front-section">
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/tag.svg" width="30" height="30">
                <h2><?php echo get_cat_name(get_query_var('cat'));?></h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div id="posts" class="row no-gutters" data-max-page="<?php echo $category_posts->max_num_pages;?>" data-page-type="archive" data-cat="<?php echo get_query_var('cat');?>">
                    <?php if ( $category_posts->have_posts() ) {
                    while ( $category_posts->have_posts() ) {
                            $category_posts->the_post(); ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-post__wrapper fade">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? }
                    } else {
                        // No posts
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if ( $category_posts->max_num_pages > 1 ) { ?>
                <div class="section-button">
                    <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15" style="margin: 0px 7px 0px 0px;"><span>Загрузить ещё</span></div>
                    <div id="loader" style="display:none; margin:10px 0;"></div>
                </div>
            <? } ?>
        </div>
    </section>

<?php 
get_footer(); 
?>
