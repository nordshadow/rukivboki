<?php
/**
 * Template Name: Страница Все страны
 */
get_header();
?>

    <section>
        <div class="guide-header__wrapper container-fluid" style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(<?php echo get_the_post_thumbnail_url();?>);">
            <div class="guide-header__inner">
            <h1>Все страны</h1>
            <div class="guide-header__icon_wrapper">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/navigator.svg" width="18" height="18">
            <div class="guide-header__icon_text">Навигатор</div>
            </div>
            </div>
        </div>
    </section>

    <?php

    // Countries

    $term_asia = get_term_by('slug', 'asia', 'location'); 
    $term_asia_id = $term_asia->term_id;

    $term_africa = get_term_by('slug', 'africa', 'location'); 
    $term_africa_id = $term_africa->term_id;

    $term_america = get_term_by('slug', 'america', 'location'); 
    $term_america_id = $term_america->term_id;

    $term_europe = get_term_by('slug', 'europe', 'location'); 
    $term_europe_id = $term_europe->term_id;

    $term_oceania = get_term_by('slug', 'oceania', 'location'); 
    $term_oceania_id = $term_oceania->term_id;

    ?>

    <?php 

    // Europe

    $countries_europe = get_terms( array(
        'hide_empty'  => 0,  
        'orderby'     => 'name',
        'order'       => 'ASC',
        'taxonomy'    => 'location',
        'child_of' => $term_europe_id,
    ) );

    ?>

     <section id="europe">
        <div class="section-wrapper bg-grey container-fluid fade pb-5">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50" alt="Европа">
                <h2>Европа</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="card-container p-0">  
                    <?php

                    foreach ($countries_europe as $country_europe) {

                        $country_id = $country_europe->term_id;
                        $check_type_country = get_term_meta( $country_id, 'location-type', true );

                        if ($check_type_country == 'country') {

                            $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                            $country_img = wp_get_attachment_image_url( $country_img_id, 'medium_large' );
                            $country_flag = get_term_meta( $country_id, 'flag-url', true );
                            $country_title = get_term( $country_id )->name;
                            $country_url = get_term_link( (int)$country_id, 'location' );

                    ?>
                        <div class="card-wrapper-main card-wrapper-page m-0 fade">
                            <div class="card-wrapper">
                                <a href="<?php echo $country_url; ?>">                    
                                    <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                    <div class="card-country-title card-country-title-page"><?php echo $country_title; ?></div>
                                    <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                <?php } } ?>
                            
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <?php 

    // Asia

    $countries_asia = get_terms( array(
        'hide_empty'  => 0,  
        'orderby'     => 'name',
        'order'       => 'ASC',
        'taxonomy'    => 'location',
        'child_of' => $term_asia_id,
    ) );

    ?>

     <section id="asia">
        <div class="section-wrapper bg-white container-fluid pb-5 fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50" alt="Азия">
                <h2>Азия</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="card-container p-0">  
                    <?php

                    foreach ($countries_asia as $country_asia) {

                        $country_id = $country_asia->term_id;
                        $check_type_country = get_term_meta( $country_id, 'location-type', true );

                        if ($check_type_country == 'country') {

                            $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                            $country_img = wp_get_attachment_image_url( $country_img_id, 'large' );
                            $country_flag = get_term_meta( $country_id, 'flag-url', true );
                            $country_title = get_term( $country_id )->name;
                            $country_url = get_term_link( (int)$country_id, 'location' );

                    ?>
                        <div class="card-wrapper-main card-wrapper-page m-0 fade">
                            <div class="card-wrapper">
                                <a href="<?php echo $country_url; ?>">                    
                                    <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                    <div class="card-country-title card-country-title-page"><?php echo $country_title; ?></div>
                                    <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                <?php } } ?>
                            
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <?php 

    // America

    $countries_america = get_terms( array(
        'hide_empty'  => 0,  
        'orderby'     => 'name',
        'order'       => 'ASC',
        'taxonomy'    => 'location',
        'child_of' => $term_america_id,
    ) );

    ?>

     <section id="america">
        <div class="section-wrapper bg-grey container-fluid pb-5 fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50" alt="Америка">
                <h2>Америка</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="card-container p-0">  
                    <?php

                    foreach ($countries_america as $country_america) {

                        $country_id = $country_america->term_id;
                        $check_type_country = get_term_meta( $country_id, 'location-type', true );

                        if ($check_type_country == 'country') {

                            $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                            $country_img = wp_get_attachment_image_url( $country_img_id, 'large' );
                            $country_flag = get_term_meta( $country_id, 'flag-url', true );
                            $country_title = get_term( $country_id )->name;
                            $country_url = get_term_link( (int)$country_id, 'location' );

                    ?>
                        <div class="card-wrapper-main card-wrapper-page m-0 fade">
                            <div class="card-wrapper">
                                <a href="<?php echo $country_url; ?>">                    
                                    <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                    <div class="card-country-title card-country-title-page"><?php echo $country_title; ?></div>
                                    <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                <?php } } ?>
                            
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <?php 

    // Africa

    $countries_africa = get_terms( array(
        'hide_empty'  => 0,  
        'orderby'     => 'name',
        'order'       => 'ASC',
        'taxonomy'    => 'location',
        'child_of' => $term_africa_id,
    ) );

    ?>


    <section id="africa">
        <div class="section-wrapper bg-white container-fluid pb-5 fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50" alt="Африка">
                <h2>Африка</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="card-container p-0">  
                    <?php

                    foreach ($countries_africa as $country_africa) {

                        $country_id = $country_africa->term_id;
                        $check_type_country = get_term_meta( $country_id, 'location-type', true );

                        if ($check_type_country == 'country') {

                            $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                            $country_img = wp_get_attachment_image_url( $country_img_id, 'large' );
                            $country_flag = get_term_meta( $country_id, 'flag-url', true );
                            $country_title = get_term( $country_id )->name;
                            $country_url = get_term_link( (int)$country_id, 'location' );

                    ?>
                        <div class="card-wrapper-main card-wrapper-page m-0 fade">
                            <div class="card-wrapper">
                                <a href="<?php echo $country_url; ?>">                    
                                    <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                    <div class="card-country-title card-country-title-page"><?php echo $country_title; ?></div>
                                    <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                <?php } } ?>
                            
                    </div>
                </div>
            </div> 
        </div>
    </section>


    <?php 

    // Oceania

    $countries_oceania = get_terms( array(
        'hide_empty'  => 0,  
        'orderby'     => 'name',
        'order'       => 'ASC',
        'taxonomy'    => 'location',
        'child_of' => $term_oceania_id,
    ) );

    ?>

     <section id="oceania">
        <div class="section-wrapper bg-grey container-fluid pb-5 mb-n5 fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50" alt="Океания">
                <h2>Океания</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="card-container p-0">  
                    <?php

                    foreach ($countries_oceania as $country_oceania) {

                        $country_id = $country_oceania->term_id;
                        $check_type_country = get_term_meta( $country_id, 'location-type', true );

                        if ($check_type_country == 'country') {

                            $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                            $country_img = wp_get_attachment_image_url( $country_img_id, 'large' );
                            $country_flag = get_term_meta( $country_id, 'flag-url', true );
                            $country_title = get_term( $country_id )->name;
                            $country_url = get_term_link( (int)$country_id, 'location' );

                    ?>
                        <div class="card-wrapper-main card-wrapper-page m-0 fade">
                            <div class="card-wrapper">
                                <a href="<?php echo $country_url; ?>">                    
                                    <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                    <div class="card-country-title card-country-title-page"><?php echo $country_title; ?></div>
                                    <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                <?php } } ?>
                            
                    </div>
                </div>
            </div> 
        </div>
    </section>

<?php
get_footer();
?>