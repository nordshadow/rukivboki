﻿</main>
<footer class="footer-wrapper">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="footer-logo__wrapper">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-footer.svg" alt="РУКИ-В-БОКИ" width="215" height="65">
                        <div class="footer-logo__text">Путешествия и что мы в них находим</div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="footer-nav__wrapper">
                        <div class="footer-nav__wrapper-inner">
                            <div class="footer-nav__header">Разделы сайта</div>
                            <div class="footer-nav__list">
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/guides/">Путеводители</a></div>
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/travel/">Все страны</a></div>
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/advice/">Подготовка к поездке</a></div>
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/resources/">Ресурсы</a></div>
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/blog/">Блог</a></div>
                                <div class="footer-nav__list-item"><a href="<?php echo get_home_url(); ?>/contacts/">Контакты</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                   <div class="footer-social__wrapper">
                        <div class="footer-social__wrapper-inner">
                            <div class="footer-social__header">Социальные сети</div>
                            <div class="footer-soical__list">
                                <a href="https://www.instagram.com/elviuus/" target="_blank"><div class="footer-soical__item_wrapper"><div class="footer-soical__item-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/instagram.svg" alt="Instagram" width="20" height="20"></div></div></a>
                                <a href="https://www.facebook.com/elvi.uus/" target="_blank"><div class="footer-soical__item_wrapper"><div class="footer-soical__item-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/facebook.svg" alt="Facebook" width="10" height="10"></div></div></a>
                                <a href="https://vk.com/eusmanova/" target="_blank"><div class="footer-soical__item_wrapper"><div class="footer-soical__item-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/vk.svg" alt="VK" width="20" height="20"></div></div></a>
                                <a href="<?php echo get_home_url(); ?>/rss-feed/" target="_blank"><div class="footer-soical__item_wrapper"><div class="footer-soical__item-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/rss.svg" alt="RSS" width="20" height="20"></div></div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="footer-basement__wrapper"><strong>© 2010 - <?php echo date('Y'); ?> РУКИ-В-БОКИ</strong><span> При использовании материалов гиперссылка на сайт обязательна</span></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="overlay"></div>
<div id="search" class="">
    <div class="close-search">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/close-search.svg" alt="Закрыть" width="25" height="25">
    </div>
    <form name="search" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="search" placeholder="Что ищем?" value="<?php echo get_search_query(); ?>" name="s" id="s" autocomplete="off" required="required" />
        <button type="submit" class="button-do-search">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 57 57" fill="#fff"><path class="st0" d="M55.1,51.9L41.6,37.8C45.1,33.6,47,28.4,47,23C47,10.3,36.7,0,24,0S1,10.3,1,23s10.3,23,23,23
	c4.8,0,9.3-1.4,13.2-4.2L50.8,56c0.6,0.6,1.3,0.9,2.2,0.9c0.8,0,1.5-0.3,2.1-0.8C56.3,55,56.3,53.1,55.1,51.9z M24,6
	c9.4,0,17,7.6,17,17s-7.6,17-17,17S7,32.4,7,23S14.6,6,24,6z"/></svg>
            <span>Искать</span>
        </button>
    </form>
</div>
<div class="scroll-top" onclick='window.scrollTo({top: 0, behavior: "smooth"});'><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="18px"><path d="M177 159.7l136 136c9.4 9.4 9.4 24.6 0 33.9l-22.6 22.6c-9.4 9.4-24.6 9.4-33.9 0L160 255.9l-96.4 96.4c-9.4 9.4-24.6 9.4-33.9 0L7 329.7c-9.4-9.4-9.4-24.6 0-33.9l136-136c9.4-9.5 24.6-9.5 34-.1z"/></svg></div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(25402259, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25402259" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --> 

<!--LiveInternet counter-->
<div style="display:none">
    <script type="text/javascript"><!--
    document.write("<a href='http://www.liveinternet.ru/click' "+
    "target=_blank><img src='http://counter.yadro.ru/hit?t17.6;r"+
    escape(document.referrer)+((typeof(screen)=="undefined")?"":
    ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
    ";"+Math.random()+
    "' alt='' title='LiveInternet: показано число просмотров за 24"+
    " часа, посетителей за 24 часа и за сегодня' "+
    "border='0' width='88' height='31'><\/a>")
    </script>
</div>
<!--/LiveInternet-->

<!-- Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8603903-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

<?php wp_footer(); ?>


</body>
</html>