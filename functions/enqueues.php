<?php 

// Enqueue Scripts & Styles 

function rv_enqueues() {
    
    //global $random_number;
    //$randomizr = '' . rand(10099999990,9999999999999999);
    //wp_register_style('styles', get_template_directory_uri() . '/assets/css/styles.min.css', false, $randomizr);
    //wp_enqueue_style('styles');
    
    wp_register_style('styles', get_template_directory_uri() . '/assets/css/styles.min.css');
    wp_enqueue_style('styles');
    
    wp_register_style('bootstrap-grid', get_template_directory_uri() . '/assets/css/bootstrap-grid.css');
    wp_enqueue_style('bootstrap-grid');
        
    wp_register_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', '','1.1', true); 
    wp_enqueue_script( 'scripts' );
    
    if(is_front_page()) {
    
    wp_register_script( 'script-front', get_template_directory_uri() . '/assets/js/script-front.js', '','1.1', true); 
    wp_enqueue_script( 'script-front' );
        
    }
    
    // Swiper
    
    wp_register_script( 'swiper', get_template_directory_uri() . '/assets/js/swiper-bundle-min.js', array('jquery')); 
    wp_enqueue_script( 'swiper' );
    
    wp_register_style( 'swiper', get_template_directory_uri() .'/assets/css/swiper-bundle.min.css');
    wp_enqueue_style( 'swiper' );
    
    wp_register_script( 'fslightbox.js', get_template_directory_uri() . '/assets/js/fslightbox.js', array( 'jquery' ), false, true); 
    wp_enqueue_script( 'fslightbox.js' );

    
    wp_register_script( 'fadein', get_template_directory_uri() . '/assets/js/fadin.umd.js', array( 'jquery' ), false, true);
    wp_enqueue_script( 'fadein' );
    
    // AJAX
    
    wp_register_script( 'ajax-loadmore', get_stylesheet_directory_uri(). '/assets/js/ajax.js', array('jquery'), false, true );
    global $wp_query;
    // Localize the script with new data
    $script_data_array = array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'load_more_posts' ),
    );
    wp_localize_script( 'ajax-loadmore', 'rv', $script_data_array );
 
    // Enqueued script with localized data.
    wp_enqueue_script( 'ajax-loadmore' );
    
    
}

add_action('wp_enqueue_scripts', 'rv_enqueues', 100);

function rv_admin_enqueues() { 
    
    wp_register_style( 'admin-css', get_template_directory_uri() .'/assets/css/admin-styles.css' );
    wp_enqueue_style( 'admin-css' );
    
    wp_register_script( 'admin-js', get_template_directory_uri() . '/assets/js/admin-scripts.js', array('jquery')); 
    wp_enqueue_script( 'admin-js' );
    
    
    wp_register_style( 'editor-style-css', get_template_directory_uri() .'/assets/css/editor-style.css' );
    wp_enqueue_style( 'editor-style-css' );

}

add_action('admin_enqueue_scripts', 'rv_admin_enqueues', 100);


add_action( 'after_setup_theme', 'rv_add_editor_styles' );
function rv_add_editor_styles() {

	add_editor_style( get_template_directory_uri() .'/assets/css/editor-styles.css' );
	// необходимая поддержка add_theme_support( 'editor-style' ); активируется автоматом
}