<?php 

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');

function load_posts_by_ajax_callback() {
    
    $cat_id = $_POST['cat'];
    $page_type = $_POST['page_type'];
    $search_text = $_POST['search_text'];
    
    
    $cat = get_category($cat_id);
    $cat_slug = $cat->slug;
    
    if($cat_slug == 'innovation') {
    
    // Inovation Category

    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'category_name' => 'innovation',
        'posts_per_page' => '9',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );
    $innovations = new WP_query ( $args );
    $post_ids = wp_list_pluck( $innovations->posts, 'ID' );
    ?>
 
    <?php
                    foreach ($post_ids as $post_id) {

                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                
                    foreach ($deep_terms as $deep_term) {
                        $term_id = $deep_term->term_id;
                        $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                        if ($check_type == 'city') {
                            $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                            $innovasion_flag = get_term_meta( $parent_id, 'flag-url', true );
                            $innovasion_title = $deep_term->name;
                            $innovasion_url = get_permalink($post_id);
                            $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );

                        }
                        else if ($check_type == 'country') {
                                $innovasion_flag = get_term_meta( $term_id, 'flag-url', true );
                                $innovasion_title = $deep_term->name;
                                $innovasion_url = get_permalink($post_id);
                                $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );
                        }
                    ?>
                       <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <a href="<?php echo $innovasion_url;?>">
                                        <div class="card-text-image" style="background-image:url('<?php echo $innovasion_img; ?>')">
                                        <div class="card-flag text-right"><img src="<?php echo $innovasion_flag; ?>" width="35" height="25"></div>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gear-bold.svg" width="13" height="21"></div>
                                            <div class="card-category">
                                            <?php echo $innovasion_title; ?></div>
                                        </div>
                                        </div>
                                        <div class="card-text-title"><?php echo get_the_title($post_id);?></div>
                                        <div class="card-text-excerpt"><?php if (!empty( return_lead( $post_id ) ) ) { echo return_lead( $post_id ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php 
                  }  } 
    wp_die();
    
    
    }
    
    // Recipe Category
    
    else if($cat_slug == 'recipe') {
        
    check_ajax_referer('load_more_posts', 'security');
    
    $paged = $_POST['page'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'category_name' => 'recipe',
        'posts_per_page' => '8',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );

    $recipes = new WP_query ( $args );
    $post_ids = wp_list_pluck( $recipes->posts, 'ID' );
    ?>
    <?php
                    
                    foreach ($post_ids as $post_id) {
                         
                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                        
                        foreach ($deep_terms as $deep_term) {
                    
                            $term_id = $deep_term->term_id;
                            $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                            if ($check_type == 'city') {
                                $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                                $country_flag = get_term_meta( $parent_id, 'flag-url', true );
                                $title = $deep_term->name;

                            }
                            else if ($check_type == 'country') {
                                $country_flag = get_term_meta( $term_id, 'flag-url', true );
                                $title = $deep_term->name;
                            }
                        }
                        
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php echo get_the_permalink($post_id)?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-image" style="background: url('<?php echo get_the_post_thumbnail_url(($post_id) , 'medium_large'); ?>') no-repeat center center / cover">
                                        <div class="card-recipe-title"><?php echo get_the_title($post_id);?></div>
                                        <?php if(!empty($country_flag)) {?><div class="card-flag text-right"><img src="<?php echo $country_flag;?>" width="35" height="25"></div><?php } ?>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/icons/cooking-bold.svg" width="13" height="21"></div>
                                            <?php if(!empty($title)) {?><div class="card-category">
                                            <?php echo $title;?></div><?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php 
                } 
    wp_die();
    }

    else if($cat_slug == 'advice') {
        
    // Advices Category

    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
        
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'category_name' => 'advice',
        'posts_per_page' => '8',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );
    $advices = new WP_query ( $args );
        
                    if ( $advices->have_posts() ) {
                    while ( $advices->have_posts() ) {
                            $advices->the_post(); ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-notes-image" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover">
                                    <div class="card-notes-title"><?php the_title();?></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
    <? } } wp_die(); 
    }
    
    else if($page_type == 'blog') {
        
    // Blog page

    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
    $exlude = get_category_by_slug('guides');
    $exlude_id = $exlude->term_id;
        
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'category__not_in' => array($exlude_id),
        'posts_per_page' => '10',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );
    $latest_posts = new WP_query ( $args );

    if ( $latest_posts->have_posts() ) {
    while ( $latest_posts->have_posts() ) {
            $latest_posts->the_post(); 
    ?>
    
    <section>
        <div class="container-post__wrapper container-fluid fade">
            <div class="container-post /*container-page*/ p-0">

                <?php if (!empty(wp_get_post_terms(get_the_ID(),'location'))) {                                      
                    echo '<div class="breadcrumbs__container-blog container-fluid">';
                    post_location_breadcrumbs(); 
                    echo '</div>';
                } ?>   

                <div class="container-post__inner container-blog__inner">
                    <a href="<?php echo get_the_permalink(get_the_ID());?>">
                    <div class="post-featured-img__wrapper"> 
                        <div class="post-featured-img__inner"><?php the_post_thumbnail('large'); ?></div>
                    </div>
                    <div class="post-header__container py-3">
                        <div class="post-header__wrapper">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                    </a>
                    <?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?>
                    <div class="read-more">
                        <p><a href="<?php echo get_the_permalink($related_posts__id);?>">Читать полностью »</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    }  } 
    wp_die();
    
    
    }
    
    // Search page
    
    else if($page_type == 'search') {
        
    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
        
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '6',
        'orderby' => 'modified',
        's' => $search_text,
        'paged' => $paged,
    );
    
    $search_posts = new WP_query ( $args );

    if ( $search_posts->have_posts() ) {
    while ( $search_posts->have_posts() ) {
            $search_posts->the_post(); 
    ?>

    <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
        <a href="<?php the_permalink()?>">
            <div class="card-post__wrapper-inner">
                <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                <div class="card-post__title"><?php the_title();?></div>
                <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
            </div>
        </a>
    </div>

    <?php
    }  } 
    wp_die();

    }
    
    // Tag page
    
    else if($page_type == 'tag') {
        
    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
    $cat_id = $_POST['cat'];
        
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '6',
        'tag' => $cat_id,
        'paged' => $paged,
    );
    
    $search_posts = new WP_query ( $args );

    if ( $search_posts->have_posts() ) {
    while ( $search_posts->have_posts() ) {
            $search_posts->the_post(); 
    ?>

    <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
        <a href="<?php the_permalink()?>">
            <div class="card-post__wrapper-inner">
                <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                <div class="card-post__title"><?php the_title();?></div>
                <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
            </div>
        </a>
    </div>

    <?php
    }  } 
    wp_die();

    }
    
    // Location page
    
    else if($page_type == 'travel') {
        
    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
    $term_id = $_POST['cat'];
        
    $exlude = get_category_by_slug('guides');
        
    $exlude_id = $exlude->term_id;
    $args = array(
        'post_status' => 'publish',
        'post_type'   => 'post',
        'category__not_in' => array($exlude_id),
        'posts_per_page' => '9',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
        'tax_query' => array(
            array(
            'taxonomy' => 'location',
            'field' => 'term_id',
            'terms' => $term_id,
             )
          )
    );
    $term_posts = new WP_query ( $args );

    if ( $term_posts->have_posts() ) {
    while ( $term_posts->have_posts() ) {
            $term_posts->the_post(); 
    ?>

    <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
        <a href="<?php the_permalink()?>">
            <div class="card-post__wrapper-inner">
                <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                <div class="card-post__title"><?php the_title();?></div>
                <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
            </div>
        </a>
    </div>

    <?php
    }  } 
    wp_die();

    }
    
    // Latest Posts
    
    else if($page_type == 'latest') {
        
    check_ajax_referer('load_more_posts', 'security');
        
    /*
    $guide_cat = get_category_by_slug('guides');
    $innovation_cat = get_category_by_slug('innovation');
    $notes_cat = get_category_by_slug('advice');
    $recipe_cat = get_category_by_slug('recipe');

    $cat_not_in = array ($guide_cat->term_id, $innovation_cat->term_id, $notes_cat->term_id, $recipe_cat->term_id);
    $cat_not_in = implode(', ', $cat_not_in);
    */
        
    $exlude = get_category_by_slug('guides');
    $exlude_id = $exlude->term_id;

    $paged = $_POST['page'];
    $args = array(
        'post_type'   => 'post',
        'post_status' => 'publish',
        //'category__not_in' => array(3, 4, 6, 357),
        'category__not_in' => array(357),
        'posts_per_page' => '6',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );

    $latest_posts = new WP_query ( $args );

    if ( $latest_posts->have_posts() ) {
    while ( $latest_posts->have_posts() ) {
            $latest_posts->the_post(); 
    ?>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="<?php the_permalink()?>">
                    <div class="card-post__wrapper fade">
                        <div class="card-post__wrapper-inner">
                            <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                            <div class="card-post__title"><?php the_title();?></div>
                            <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                        </div>
                    </div>
                </a>
            </div>
        <?php 
        } }
    wp_die();
    }
    
    // Category Archive
    
    else if($page_type == 'archive') {
        
    check_ajax_referer('load_more_posts', 'security');

    $paged = $_POST['page'];
    $cat_id = $_POST['cat'];
        
    $args = array(
        'post_type'   => 'post',
        'post_status' => 'publish',
        'category__in' => $cat_id,
        'posts_per_page' => '6',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => $paged,
    );

    $category_posts = new WP_query ( $args );

    if ( $category_posts->have_posts() ) {
    while ( $category_posts->have_posts() ) {
            $category_posts->the_post(); 
    ?>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="<?php the_permalink()?>">
                    <div class="card-post__wrapper fade">
                        <div class="card-post__wrapper-inner">
                            <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                            <div class="card-post__title"><?php the_title();?></div>
                            <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                        </div>
                    </div>
                </a>
            </div>
        <?php 
        } }
    wp_die();
    }

    
}