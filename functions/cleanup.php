<?php

// Clean up wordpres <head>

remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version
remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Remove RSS Feeds

function itsme_disable_feed() {
    wp_redirect( home_url().'/rss-feed/' );
    die;
}

add_action('do_feed', 'itsme_disable_feed', 1);
add_action('do_feed_rdf', 'itsme_disable_feed', 1);
add_action('do_feed_rss', 'itsme_disable_feed', 1);
add_action('do_feed_rss2', 'itsme_disable_feed', 1);
add_action('do_feed_atom', 'itsme_disable_feed', 1);
add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);


// Remove type="text/javascript" from <script> and type=”text/css” from <style>

add_filter( 'style_loader_tag', 'remove_type_attr');
add_filter( 'script_loader_tag', 'remove_type_attr');
function remove_type_attr($src) {
	return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $src );
}

// Remove "This site is optimized with the Yoast SEO plugin"

add_action( 'template_redirect', function () {
    if ( ! class_exists( 'WPSEO_Frontend' ) ) {
        return;
    }
    $instance = WPSEO_Frontend::get_instance();
    // make sure, future version of the plugin does not break our site.
    if ( ! method_exists( $instance, 'debug_mark') ) {
        return ;
    }
     remove_action( 'wpseo_head', array( $instance, 'debug_mark' ), 2 );
}, 9999 );

// Disable Yoast Generated Schema Data
add_filter( 'wpseo_json_ld_output', '__return_false' );