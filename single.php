<?php
/**
 * Template Name: Single Post
 */
get_header();

?>
    
    <section itemscope itemtype="https://schema.org/BlogPosting">
    <div itemprop="mainEntityOfPage" content="<?php echo the_permalink() ?>"></div>
    <div class="breadcrumbs__container container-fluid">
        <?php if (!empty(post_location_breadcrumbs())) { ?>
        <?php post_location_breadcrumbs(); ?>   
        <?php } ?>
    </div>
    <div class="post-header__container container-fluid">
        <div class="post-header__wrapper">
            <h1 itemprop="headline name"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="post-info__container container-fluid">
        <div class="post-info__wrapper">
            <div class="d-none" itemprop="datePublished" content="<?php echo get_the_date('c'); ?>"></div>
            <div class="d-none" itemprop="dateModified" content="<?php echo get_the_modified_date('c'); ?>"></div>
            <?php $checkbox_modified = get_post_meta( $post->ID ); ?>
            <div class="d-lg-flex <?php if ((get_the_time('j F Y') !== get_the_modified_date('j F Y')) && $checkbox_modified['show_modified'][0] !== 'no') { echo 'd-sm-none d-none'; }?> post-info__date">
                <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/calendar.svg">
                <div class="post-info__column">
                    <span class="post-info__column-head">Опубликовано:</span>
                    <span class="post-info__column-value"><?php echo get_the_time('j F Y'); ?></span>
                </div>
            </div>

            <?php 
            
            
            if ((get_the_time('j F Y') !== get_the_modified_date('j F Y')) && $checkbox_modified['show_modified'][0] !== 'no') { ?>

            <div class="post-info__edit">
                <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/edit.svg">
                <div class="post-info__column">
                    <span class="post-info__column-head">Обновлено:</span>
                    <span class="post-info__column-value"><?php echo get_the_modified_date('j F Y'); ?></span>
                </div>
            </div>

            <?php } ?>

            <div class="post-info__user">
                <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/user.svg">
                <div class="post-info__column">
                    <? $post_authors = wp_get_post_terms(get_the_ID(), 'users', array('fields' => 'names')); 
                    if ( ! empty( $post_authors ) ) {
                        
                        $count = count($post_authors);
                        $output = array();
                        
                        foreach($post_authors as $post_author) {
                            $output[] = $post_author;
                            unset($post_author);
                        }
                    ?>
                    <span class="post-info__column-head"><?php if($count > 1) { echo 'Авторы:'; } else { echo 'Автор:'; }?></span>
                    <span class="post-info__column-value" itemprop="author">
                        
                        <?php
                        
                        echo implode("<br>", $output);

                        }
                        
                    else { wp_set_post_terms( get_the_ID(), 'Эльви Усманова', 'users', true );

                    }
                        
                    ?>
                    </span>
                </div>
            </div>
        </div> 
    </div>
    <div class="post-featured-img__wrapper container-fluid" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
        <div class="d-none" itemprop="url image"><?php echo the_post_thumbnail_url(); ?></div>
        <div class="post-featured-img__inner"><?php the_post_thumbnail('large'); ?></div>
    </div>
    <div class="container-post__wrapper container-fluid p-0">
        <div class="container-post">
            <div class="container-post__inner">
                <?php //echo show_guide_menu(get_the_ID());?>
                <?php 
                
                if (!empty( return_lead( get_the_ID() ) ) ) { echo '<p itemprop="description"><strong>' . return_lead( get_the_ID() ) .  '</strong></p>'; }
                
                $post_lead_hr = get_post_meta( get_the_ID() , 'lead_meta_box_hr');
                if ($post_lead_hr[0] == 'yes') { echo '<hr>'; }
                
                ?>
                <div itemprop="articleBody">
                <?php the_content(); ?>
                </div>
                
                <!-- Photo Source -->
                <?php 
                $related_posts = get_field('photo_source', get_the_ID());
                //if( $related_posts && in_array( '1', $related_posts )) {
                if( $related_posts[0] == '1' ) {
                    
                ?>
                <div class="photo-source__container">
                    <div class="photo-source__wrapper">
                        <div class="photo-source__source">
                            <img class="photo-source__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/photo.svg">
                            <div class="photo-source__row">
                                <span class="photo-source__row-head">Фотографии:</span>
                                
                                <?php
                                    
                                    $photo_source_values = get_field('photo_source_value');
                                      if( $photo_source_values ) { ?>
                                          <span class="photo-source__row-value">
                                              <?php
                                          
                                                foreach( $photo_source_values as $photo_source_value ) {

                                                    if ($photo_source_value == 'author') {


                                                        $photo__author_values = get_field('photo__author-value');
                                                        $authors = [];

                                                        foreach( $photo__author_values as $photo__author_value ) {


                                                            $author = get_term($photo__author_value, 'users');
                                                            $author_name = $author->name;
                                                            //echo $author_name;
                                                            $authors[] = $author_name;
                                                        }

                                                    }

                                                    else if ($photo_source_value == 'site') {

                                                        $sites = [];

                                                        while( have_rows('photo__site-repeater') ) : the_row();

                                                                // Get parent value.
                                                                $photo__site_group = get_sub_field('photo__site-group');


                                                                // Loop over sub repeater rows.
                                                                if( have_rows('photo__site-group')) {
                                                                    while( have_rows('photo__site-group') ) : the_row();

                                                                        // Get sub value.
                                                                        $photo__site_title = get_sub_field('photo__site-title');
                                                                        $photo__site_url = get_sub_field('photo__site-url');

                                                                        if(!empty($photo__site_url)) {

                                                                        $sites[] = '<a href="'.$photo__site_url.'">'.$photo__site_title.'</a>';

                                                                        }

                                                                        else {

                                                                        $sites[] = $photo__site_title;

                                                                        }

                                                                    endwhile;
                                                                }
                                                            endwhile;
                                                    }
                                                    
                                                    else if ($photo_source_value == 'photographer') {

                                                        $photographers = [];

                                                        while( have_rows('photo__photographer-repeater') ) : the_row();

                                                                // Get parent value.
                                                                $photo__photographer_group = get_sub_field('photo__photographer-group');


                                                                // Loop over sub repeater rows.
                                                                if( have_rows('photo__photographer-group')) {
                                                                    while( have_rows('photo__photographer-group') ) : the_row();

                                                                        // Get sub value.
                                                                        $photo__photographer_name = get_sub_field('photo__photographer-name');
                                                                        $photo__photographer_url = get_sub_field('photo__photographer-url');

                                                                        if(!empty($photo__photographer_url)) {

                                                                        $photographers[] = '<a href="'.$photo__photographer_url.'">'.$photo__photographer_name.'</a>';

                                                                        }

                                                                        else {

                                                                        $photographers[] = $photo__photographer_name;

                                                                        }

                                                                    endwhile;
                                                                }
                                                            endwhile;
                                                    }
                                                    
                                                    
                                                    
                                                }
                                            } 
                                            $sources = array_merge((array)$authors, (array)$photographers, (array)$sites);
                                            $sources = implode(', ', $sources);
                                            echo $sources;
                                              
                                            ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php  } else { echo ''; } ?>
                
                <!-- Tags -->
                <div class="post-tags__container">
                    <div class="post-tags__wrapper">
                        <?php if(!empty(get_the_tag_list())) { ?><div class="d-none" itemprop="keywords" content="<?php echo implode( ', ', array_map( function( $tag ){ return $tag->name;}, get_the_tags() ) );?>"></div><?php }?>
                    <?php echo get_the_tag_list();?>
                    </div>
                </div>
                
                <!-- Related Posts -->
                <?php 
                $related_posts = get_field('related-posts');
                $related_posts__ids = get_field('related-posts__ids');
                
                if ($related_posts[0] == '1') {
                    // check if array has values
                    if ($related_posts__ids !== []) {
                ?>

                <div class="related-posts__container">
                    <div class="related-posts__wrapper">
                        <div class="related-posts__heading">
                            <!-- <img class="related-posts__icon" src="<?php //echo get_template_directory_uri(); ?>/assets/img/book.svg"> -->
                            <div class="related-posts__column">
                                <span class="related-posts__column-head">Смотрите также:</span>
                            </div>
                        </div>
                
                        <div class="related-posts__list-wrapper">
                            <ul>
                            <?php
                            foreach ($related_posts__ids as $related_posts__id) {
                            ?>
                                <li><a href="<?php echo get_the_permalink($related_posts__id);?>"><?php echo get_the_title($related_posts__id);?></a></li>
                            <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php  } } ?>

                <div class="d-none" itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
                <link class="d-none" itemprop="sameAs" href="<?php echo get_home_url(); ?>" />
                <div class="d-none" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject"> 
                <link class="d-none" itemprop="url image" href="<?php echo get_template_directory_uri() .'/assets/img/rv_snippet.png'?>"/>
                </div> 
                <div class="d-none" itemprop="name" content="<?php bloginfo('name');?>"></div>
                <div class="d-none" itemprop="address" content="<?php bloginfo('name');?>"></div>
                <div class="d-none" itemprop="telephone" content="<?php echo get_home_url(); ?>"></div>
                </div>
               
                <div class="social-buttons__container">
                <!------ Rambler.Likes script start ------>
                <div class="rambler-share"></div>
                    <script>
                    (function() {
                    var init = function() {
                    RamblerShare.init('.rambler-share', {
                        "utm": "utm_medium=social",
                        "counters": true,
                        "buttons": [
                            "vkontakte",
                            "facebook",
                            "odnoklassniki",
                            "telegram",
                            "viber",
                            "whatsapp"
                        ]
                    });
                    };
                    var script = document.createElement('script');
                    script.onload = init;
                    script.async = true;
                    script.src = 'https://developers.rambler.ru/likes/v1/widget.js';
                    document.head.appendChild(script);
                    })();
                    </script>
                    <!------   Rambler.Likes script end  ------>
                
                </div>
                <div class="comments__container">
                <div class="comments__heading">Комментарии:</div>
                <!------ Cackle comments start------>
                    <div id="mc-container"></div>
                    <script type="text/javascript">
                    cackle_widget = window.cackle_widget || [];
                    cackle_widget.push({widget: 'Comment', id: 76698});
                    (function() {
                        var mc = document.createElement('script');
                        mc.type = 'text/javascript';
                        mc.async = true;
                        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                    })();
                    </script>
                    <a id="mc-link" href="http://cackle.me">Комментарии для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>
                    <!------ Cackle comments end------>
                </div>
            </div>
        </div>
    </div>
    </section>


    <!-- Related Posts -->

    <?php

    $post_terms =  wp_get_object_terms( get_the_ID(), 'location' ); 
    $post_deep_terms = get_deep_child_terms( $post_terms ); 

    foreach ($post_deep_terms as $post_deep_term) { 
        $term_id = $post_deep_term->term_id;
        $term_location_type = get_term_meta( $post_deep_term->term_id, 'location-type', true );
    }

    $cats = array();
        foreach (get_the_category($post_id) as $c) {
        $cat = get_category($c);
        array_push($cats, $cat->term_id);
    }


    if ($term_location_type == 'city' ) { 

    $get_country_posts = wp_get_term_taxonomy_parent_id($term_id, 'location'); 

    }
    else { $get_country_posts = $term_id; }

    $country_term_posts = get_the_terms( $get_country_posts , 'location' );
    $country_term_posts_args = array(
    'post_type'   => 'post',
    'posts_per_page' => '-1',
        'tax_query' => array(
                array(
                'taxonomy' => 'location',
                'field' => 'term_id',
                'terms' => $get_country_posts,
                 ),
            ),
    );
    $country_term_posts = count( get_posts( $country_term_posts_args ) );
    $min_posts = '6';

    if ( $country_term_posts >= $min_posts  ) {

    $args = array(
        'post_type'   => 'post',
        'posts_per_page' => '6',
        'post__not_in' => array(get_the_ID()),
        'orderby' => 'rand',
            'tax_query' => array(
                array(
                'taxonomy' => 'location',
                'field' => 'term_id',
                'terms' => $get_country_posts,
                'include_children' => 'false',
                 ),
            ),
        );
        $more_term_posts = new WP_query ( $args );

     }

    else {

        $args = array(
        'post_type'   => 'post',
        'category__not_in' => $term_id,
        'category__in' => $cats,
        'post__not_in' => array(get_the_ID()),
        'posts_per_page' => '6',
        'orderby' => 'rand',

        );
        $more_term_posts = new WP_query ( $args );

    }
    ?>
    <?php if ( $more_term_posts->have_posts() ) { ?>
    <section class="mt-3">
        <div class="section-wrapper container-post">
            <div class="section-header">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/furs.svg" width="60" height="40">
                <h2>Что еще почитать:</h2>
            </div>
        <div class="section-content fade">
            <?php echo get_guide_terms()?>
            <div class="container">
                <div class="row no-gutters">
                    <?php
                    while ( $more_term_posts->have_posts() ) {
                            $more_term_posts->the_post(); ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-post__wrapper fade">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium_large'); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            </div>
        </div>
    </section>
    <? 
    } else {
        // Постов не найдено
    }
    wp_reset_postdata(); ?>

<?php 
get_footer(); 
?>