<?php
/**
 * Template Name: Категория Рецепты
 */
get_header();
?>

<?php

    // Recipe

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'recipe',
        'posts_per_page' => '8',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => 1,
        );

    $recipes = new WP_query ( $args );
    $post_ids = wp_list_pluck( $recipes->posts, 'ID' );
    ?>

    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/cooking.svg" width="40" height="40">
                <h2>Рецепты</h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div id="posts" class="row no-gutters" data-max-page="<?php echo $recipes->max_num_pages;?>" data-cat="<?php echo get_query_var('cat');?>">
                    
                    <?php
                    
                    foreach ($post_ids as $post_id) {
                         
                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                        
                        foreach ($deep_terms as $deep_term) {
                    
                            $term_id = $deep_term->term_id;
                            $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                            if ($check_type == 'city') {
                                $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                                $country_flag = get_term_meta( $parent_id, 'flag-url', true );
                                $title = $deep_term->name;

                            }
                            else if ($check_type == 'country') {
                                $country_flag = get_term_meta( $term_id, 'flag-url', true );
                                $title = $deep_term->name;
                            }
                        }
                        
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php echo get_the_permalink($post_id)?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-image" style="background: url('<?php echo get_the_post_thumbnail_url(($post_id) , 'medium_large'); ?>') no-repeat center center / cover">
                                        <div class="card-recipe-title"><?php echo get_the_title($post_id);?></div>
                                        <?php if(!empty($country_flag)) {?><div class="card-flag text-right"><img src="<?php echo $country_flag;?>" width="35" height="25"></div><?php } ?>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/icons/cooking-bold.svg" width="13" height="21"></div>
                                            <?php if(!empty($title)) {?><div class="card-category">
                                            <?php echo $title;?></div><?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? 
                    }
                       
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if (  $recipes->max_num_pages > 1 ) { ?>
                <div class="section-button">
                    <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                    <div id="loader" style="display:none; margin:10px 0;"></div>
                </div>
            <? } ?>
        </div>
    </section>

<?php 
get_footer(); 
?>