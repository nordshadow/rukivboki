window.onscroll = function(){
    var top =	 window.pageYOffset || document.documentElement.scrollTop;
    if (top > 240) {
        document.querySelector('.header__logo-front').classList.add('js__header__logo-front');
        document.getElementById('site-header').classList.remove('logo-offset');
    } else {
        document.querySelector('.header__logo-front').classList.remove('js__header__logo-front');
        document.getElementById('site-header').classList.add('logo-offset');
    }
};

var swiper_guide = new Swiper('.swiper_guide', {
  // Optional parameters
    direction: 'horizontal',
    slidesPerView: 3,
    loop: true,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },

    mousewheel: true,
    breakpoints: {
      0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        576: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2
        },
        992: {
          slidesPerView: 3
        },
      1200: {
          slidesPerView: 3
        },
      }
})

var swiper_country = new Swiper('.swiper_country', {
  // Optional parameters
    direction: 'horizontal',
    slidesPerView: 4,
    loop: true,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },

    mousewheel: true,
    breakpoints: {
      0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        576: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2
        },
        1200: {
          slidesPerView: 3
        },
        1400: {
          slidesPerView: 4
        },
      }
})
   