jQuery(function($) {
    var page = 2;
    var max_page = $('#posts').attr('data-max-page');
    var cat = $('#posts').attr('data-cat');
    var page_type = $('#posts').attr('data-page-type');
    var search_text = $('#posts').attr('data-search-text');
    
    $('body').on('click', '.loadmore', function() {
        $.ajax({
				url : '/wp-admin/admin-ajax.php',
				type: 'POST',
				data:{ 
					'action': 'load_posts_by_ajax',
                    'page': page,
                    'cat' : cat,
                    'page_type' : page_type,
                    'search_text' : search_text,
                    'security': rv.security,
            
				},
				beforeSend: function() {
                    if(page == max_page) { 
                        $('.loadmore').remove(); 
                    }
                        $('#loader').show();
                        $('.loadmore').hide();
				},
				success:function(result){
				    if($.trim(result) != '') {
                        $('#posts').append(result);
                        fadin('.fade');
                        page++;
				    }
                    

				}, 
                complete: function() {
                    $('#loader').hide();
                    $('.loadmore').show();
				},
        });
    });
});