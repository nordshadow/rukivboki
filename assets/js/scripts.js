document.addEventListener("DOMContentLoaded", () => {
  fadin('.fade')
});

document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {//27 is the code for escape
 var search_window = document.querySelector('#search');       
        search_window.classList.remove('open-search');
    }
};

let search_window = document.querySelector('#search');
let search_btn = document.querySelector('#search-modal');
let input = document.querySelector('input[type="search"]');    
    search_btn.addEventListener('click', function(e) {
    event.preventDefault();
    search_window.classList.add('open-search');    
    input.focus();
});

let close_search = document.querySelector('#search .close-search')
    close_search.addEventListener('click', function(event) {
    search_window.classList.remove('open-search');
});
     
     
// Show hide menu//

(function(){

  var doc = document.documentElement;
  var w = window;

  var prevScroll = w.scrollY || doc.scrollTop;
  var curScroll;
  var direction = 0;
  var prevDirection = 0;

  var header = document.getElementById('site-header');

  var checkScroll = function() {

    /*
    ** Find the direction of scroll
    ** 0 - initial, 1 - up, 2 - down
    */

    curScroll = w.scrollY || doc.scrollTop;
    if (curScroll > prevScroll) { 
      //scrolled up
      direction = 2;
    }
    else if (curScroll < prevScroll) { 
      //scrolled down
      direction = 1;
    }

    if (direction !== prevDirection) {
      toggleHeader(direction, curScroll);
    }
    
    prevScroll = curScroll;
  };

  var toggleHeader = function(direction, curScroll) {
    if (direction === 2 && curScroll > 52) { 
      
      header.classList.add('hide-v');
      prevDirection = direction;
    }
    else if (direction === 1) {
      header.classList.remove('hide-v');
      prevDirection = direction;
    }
  };
  
  window.addEventListener('scroll', checkScroll);

})();


let toggle = document.querySelector('#menu-toggle'),
    close = document.querySelector('#menu-close'),
    sidebar = document.querySelector('#nav-menu'),
    body = document.querySelector('body');

toggle.addEventListener('click', function(e) {
    e.preventDefault();
    sidebar.classList.toggle('show');
    body.classList.toggle('overflow-hidden');
    
});
close.addEventListener('click', function(e) {
    e.preventDefault();
    sidebar.classList.remove('show');
    body.classList.toggle('overflow-hidden');
}); 


document.addEventListener("DOMContentLoaded", function(event) {
  let toggle = document.querySelector('#menu-toggle'),
    close = document.querySelector('#menu-close'),
    sidebar = document.querySelector('#nav-menu'),
    body = document.querySelector('body');

  function resize() {
    if (window.innerWidth > 971) {
        sidebar.classList.remove('show');
        body.classList.remove('overflow-hidden');
    } 
  }

window.onresize = resize;
});

let resizeTimer;
  window.addEventListener("resize", () => {
  document.body.classList.add("resize-animation-stopper");
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(() => {
    document.body.classList.remove("resize-animation-stopper");
  }, 400);
});

let title = document.querySelectorAll(".header-nav__item--location, .header-nav__item--guide");

for(let i = 0; i < title.length; i++){
  title[i].addEventListener("click", function(){
    let submenu = this.querySelector('.header-nav__sub-menu'),
        link = this.querySelector('.header-nav__link'),
        chevron = this.querySelector('.header-nav__chveron-down-wrapper');
        submenu.classList.toggle("active");
        link.classList.toggle("active");
        chevron.classList.toggle("active");

  })
}
