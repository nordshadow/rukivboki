<?php
/**
 * Template Name: Single Page
 */
get_header();
?>
    <section>
        <div class="container-post__wrapper container-fluid fade">
            <div class="container-post p-0">

                <div class="post-header__container pb-0 pt-3">
                    <div class="section-header p-4 mr-0">
                    <?php if (is_page('resources')) { echo '<img src="'. get_template_directory_uri() .'/assets/icons/resources.svg" width="50" height="50">';} else if (is_page('contacts')) { echo '<img src="'. get_template_directory_uri() .'/assets/icons/contacts.svg" width="50" height="50">';} ?>
                    <h1><?php echo get_the_title();?></h1>
                    </div>
                </div>
                
                <div class="post-featured-img__wrapper pt-3"> 
                        <div class="post-featured-img__inner pt-0"><?php the_post_thumbnail('large'); ?></div>
                    </div>
                
                <div class="container-post__inner container-blog__inner pt-0">
                    <?php the_content();?>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>