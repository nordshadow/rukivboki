<?php
/**
 * Template Name: Категория Советы
 */
get_header();
?>
    <?php

    // Advice

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'advice',
        'posts_per_page' => '8',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => 1,
        );

    $advices = new WP_query ( $args );

    ?>

    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/suitcase.svg" width="50" height="50">
                <h2>Подготовка к поездке</h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div id="posts" class="row no-gutters" data-max-page="<?php echo $advices->max_num_pages;?>" data-cat="<?php echo get_query_var('cat');?>">
                    <?php if ( $advices->have_posts() ) {
                    while ( $advices->have_posts() ) {
                            $advices->the_post(); ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-notes-image" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover">
                                    <div class="card-notes-title"><?php the_title();?></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? }
                    } else {
                        // No posts
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if ( $advices->max_num_pages > 1 ) { ?>
                <div class="section-button">
                    <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                    <div id="loader" style="display:none; margin:10px 0;"></div>
                </div>
            <? } ?>
        </div>
    </section>

<?php 
get_footer(); 
?>
