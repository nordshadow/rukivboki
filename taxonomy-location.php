<?php
/**
 * Template Name: Taxonomy Location
 */
get_header();
?>
<?php

function location_breadcrumbs () {

        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $term_id = $term->term_id; 
        $term_title = $term->name; 
        $check_type = get_term_meta( $term_id, 'location-type', true );
        
        $breadcrumbs .= '<section>';
        $breadcrumbs .= '<div class="breadcrumbs__container container-fluid">';
        $breadcrumbs .= '<div class="breadcrumbs__wrapper">';
        
            if ($check_type == 'country') {
                
                        $country_flag = get_term_meta( $term_id, 'flag-url', true );
                        $country_title = $term->name;
                        $country_url = get_term_link($term_id); 
                
                        if(!empty($country_flag)) {
                            $breadcrumbs .= '<img src="' . esc_attr( $country_flag ). '">';
                        }

                        $breadcrumbs .= '<div class="breadcrumbs__item">';
                        $breadcrumbs .= '<a href="' . $country_url .'">';
                        $breadcrumbs .= $country_title ;
                        $breadcrumbs .= '</a>' . "\n";
                        $breadcrumbs .= '</div>' . "\n";
                }
        
                else if ($check_type == 'city') {
                    
                        $country_id = wp_get_term_taxonomy_parent_id($term_id, 'location');

                        $country_term = get_term( $country_id, 'location');
                        $country_flag = get_term_meta( $country_id, 'flag-url', true );

                        $country_title = $country_term->name;
                        $country_url = get_term_link($country_term->term_id); 
                    
                        $city_title = $term->name;
                        $city_url = get_term_link($term_id);     

                        if(!empty($country_flag)) {
                            $breadcrumbs .= '<img src="' . esc_attr( $country_flag ). '">';
                        }

                        $breadcrumbs .= '<div class="breadcrumbs__item">';
                        $breadcrumbs .= '<a href="' . $country_url .'">';
                        $breadcrumbs .= $country_title ;
                        $breadcrumbs .= '</a>';
                        $breadcrumbs .= '<span> / </span>';
                        $breadcrumbs .= '<a href="' . $city_url .'">';
                        $breadcrumbs .= $city_title ;
                        $breadcrumbs .= '</a>';
                        $breadcrumbs .= '</div>';
            }
        
        $breadcrumbs .= '</div>';
        $breadcrumbs .= '</div>';
        $breadcrumbs .= '</section>';
    
    return $breadcrumbs;
}
echo location_breadcrumbs();
?>
<?php

    // Get taxonomy info & image

    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id = $term->term_id; 
    $term_title = $term->name;   
    $term_img_id = get_term_meta( $term_id, '_thumbnail_id', 1 );
    $term_img_url = wp_get_attachment_image_url( $term_img_id, 'full' );

    $term_location_type = get_term_meta( $term_id, 'location-type', true );

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'guides', 
        'tax_query' => array(
            array(
            'taxonomy' => 'location',
            'field' => 'term_id',
            'terms' => $term_id,
             )
          )
    );
    $term_posts = new WP_query ( $args );

    // Get all taxonomy posts

    if($term_posts->have_posts()) {
        
        while ( $term_posts->have_posts() ) : 
        
        $term_posts->the_post();
        
        $post_id = get_the_ID();
        $post_img_url = get_the_post_thumbnail_url( $post_id, 'full' );
        
        $post_terms =  wp_get_object_terms( $post_id, 'location' ); 
        $post_deep_terms = get_deep_child_terms( $post_terms ); 
        
        foreach ($post_deep_terms as $post_deep_term) { 

            $location_type = get_term_meta( $post_deep_term->term_id, 'location-type', true );
            
            if ($location_type === 'city') {
                
                $cities[] = array(
    
                'post_id' => get_the_ID(),
                'term_id' => $post_deep_term->term_id,
                );

            }
            
            if ( $post_deep_term->term_id === $term_id ) { // Check current term guide has post
                
                $guide_post_term = $post_deep_term->term_id; 
                $guide_post_id = get_the_ID(); // Get current term guide post ID
                
            }
            
            else {  // do nothing 
            
            }
            
        }

        endwhile;
        
        $header .= '<section>';
        $header .= '<div class="guide-header__wrapper container-fluid" style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(';

        if(!empty($term_img_url)) { $header .= $term_img_url; } else { $header .= $post_img_url; }

        $header .= ');">';
        $header .= '<div class="guide-header__inner">';
        $header .= '<h1>' . $term_title . '</h1>';

        if($term_id === $guide_post_term) {
        $header .= '<a href="' . get_home_url() . '/guides/">';
        $header .= '<div class="guide-header__icon_wrapper">';
        $header .= '<img src="' . get_template_directory_uri() .'/assets/icons/location.svg" width="13" height="21">';
        $header .= '<div class="guide-header__icon_text">Путеводитель</div>';
        $header .= '</div>';
        $header .= '</a>';

        }
        
        else {
        $header .= '<a href="' . get_home_url() . '/travel/">';
        $header .= '<div class="guide-header__icon_wrapper">';
        $header .= '<img src="' . get_template_directory_uri() . '/assets/icons/navigator.svg" width="18" height="18">';
        $header .= '<div class="guide-header__icon_text">Навигатор</div>';
        $header .= '</div>';
        $header .= '</a>';
            
        }

        $header .= '</div>';
        $header .= '</div>';
        $header .= '</section>';

        echo $header;

        if(!empty($cities) && $term_location_type === 'country') {

            $section_cities .= '<section>';
            $section_cities .= '<div class="section-wrapper bg-white container-fluid">';
            $section_cities .= '<div class="section-header pb-0">';
            $section_cities .= '<img src="' . get_template_directory_uri() .'/assets/icons/guide.svg" width="50" height="50">';
            $section_cities .= '<h1>Города</h1>';
            $section_cities .= '</div>';
            $section_cities .= '<div class="section-content">';
            $section_cities .= '<div class="container">';
            $section_cities .= '<div class="card-container">';   

            foreach ($cities as $city) {

                $city_term_id = $city['term_id'];
                $city_post_id = $city['post_id'];

                $city_term_name = get_term($city_term_id)->name;
                $city_term_url = get_term_link($city_term_id); 

                $city_term_img_id = get_term_meta( $city_term_id, '_thumbnail_id', 1 );
                $city_term_img_url = wp_get_attachment_image_url( $city_term_img_id, 'medium_large' );
                $city_post_img_url = get_the_post_thumbnail_url( $city_post_id , 'medium_large' );

                $country_id = wp_get_term_taxonomy_parent_id( $city_term_id, 'location' );
                $country_name = get_term($country_id)->name;
                $country_flag = get_term_meta( $country_id, 'flag-url', true );

                if($city_term_img_url) { $bg_image = $city_term_img_url; } else { $bg_image = $city_post_img_url; }
                
                $section_cities .= '<div class="card-wrapper-main card-wrapper-page fade">';
                $section_cities .= '<div class="card-wrapper">';

                $section_cities .= '<a href="'. $city_term_url .'">';
                $section_cities .= '<div class="card-image" style="background-image:url('. $bg_image .')">';
                $section_cities .= '<div class="card-flag text-right"><img src="'. $country_flag .'" width="35" height="25"></div>';
                $section_cities .= '<div class="card-title">'. $city_term_name .'</div>';

                $section_cities .= '<div class="card-category-wrapper">';

                $section_cities .= '<div class="card-icon">';
                $section_cities .= '<img src="' . get_template_directory_uri() .'/assets/icons/location.svg" width="13" height="21">';
                $section_cities .= '</div>';

                $section_cities .= '<div class="card-category">Путеводитель</div>';
                $section_cities .= '</div>';
                $section_cities .= '</div>';
                $section_cities .= '</a>';

                $section_cities .= '</div>';
                $section_cities .= '</div>';

            }
        
            $section_cities .= '</div>';
            $section_cities .= '</div>';
            $section_cities .= '</section>';

            echo $section_cities;
            
            $exlude = get_category_by_slug('guides');
            $exlude_id = $exlude->term_id;
            if($term_id !== $guide_post_term) {
            $args = array(
                'post_status' => 'publish',
                'post_type'   => 'post',
                'category__not_in' => array($exlude_id),
                'orderby' => 'date',
                'posts_per_page' => '9',
                'orderby' => 'modified', 
                'order' => 'DESC',
                'paged' => 1,
                'tax_query' => array(
                    array(
                    'taxonomy' => 'location',
                    'field' => 'term_id',
                    'terms' => $term_id,
                     )
                  )
            );
        $other_term_posts = new WP_query ( $args );
        ?>
        
        <!-- Related Posts -->
        <?php if ( $other_term_posts->have_posts() ) {?>
        <section>
            <div class="section-wrapper container-fluid fade">
                <div class="section-header-front">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/furs.svg" width="60" height="40">
                    <h2>Публикации</h2>
                </div>
            </div>
            <div class="section-content fade">
                <div class="container">
                    <div id="posts" class="card-container" data-max-page="<?php echo $other_term_posts->max_num_pages;?>" data-cat="<?php echo $term_id;?>" data-page-type="travel">
                        <?php
                        while ( $other_term_posts->have_posts() ) {
                                $other_term_posts->the_post(); ?>

                                <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
                                    <a href="<?php the_permalink()?>">
                                        <div class="card-post__wrapper-inner">
                                            <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                            <div class="card-post__title"><?php the_title();?></div>
                                            <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                        </div>
                                    </a>
                                </div>
                            
                        <? }
                        } else {
                            // No posts
                        }
                        wp_reset_postdata(); ?>
                    </div>
                    <?php if ( $other_term_posts->max_num_pages > 1 ) { ?>
                        <div class="section-button">
                            <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                            <div id="loader" style="display:none; margin:10px 0;"></div>
                        </div>
                    <? } ?>
                </div>
            </div>
        </section>
<?php
                
                
            }

        }

    }

    else { // No guides
        
        $header .= '<section>';
        $header .= '<div class="guide-header__wrapper container-fluid" style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(' . $term_img_url . ');">';
        $header .= '<div class="guide-header__inner">';
        $header .= '<h1>' . $term_title . '</h1>';
        $header .= '<a href="' . get_home_url() . '/travel/">';
        $header .= '<div class="guide-header__icon_wrapper">';
        $header .= '<img src="' . get_template_directory_uri() . '/assets/icons/navigator.svg" width="18" height="18">';
        $header .= '<div class="guide-header__icon_text">Навигатор</div>';
        $header .= '</div>';
        $header .= '</a>';
        $header .= '</div>';
        $header .= '</div>';
        $header .= '</section>';
    
        echo $header;
        
        $exlude = get_category_by_slug('guides');
        $exlude_id = $exlude->term_id;
        if($term_id !== $guide_post_term) { // No guides, but have posts
            $args = array(
                'post_status' => 'publish',
                'post_type'   => 'post',
                'category__not_in' => array($exlude_id),
                'posts_per_page' => '9',
                'orderby' => 'modified', 
                'order' => 'DESC',
                'paged' => 1,
                'tax_query' => array(
                    array(
                    'taxonomy' => 'location',
                    'field' => 'term_id',
                    'terms' => $term_id,
                     )
                  )
            );
        $other_term_posts = new WP_query ( $args );
        ?>
        
        <!--  Posts -->
        <?php if ( $other_term_posts->have_posts() ) {?>
        <section>
            <div class="section-wrapper container-fluid fade">
                <div class="section-header-front">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/furs.svg" width="60" height="40">
                    <h2>Публикации</h2>
                </div>
            </div>
            <div class="section-content fade">
                <div class="container">
                    <div id="posts" class="card-container p-0" data-max-page="<?php echo $other_term_posts->max_num_pages;?>" data-cat="<?php echo $term_id;?>" data-page-type="travel">
                        <?php
                        while ( $other_term_posts->have_posts() ) {
                                $other_term_posts->the_post(); ?>

                            <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
                                    <a href="<?php the_permalink()?>">
                                        <div class="card-post__wrapper-inner">
                                            <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                            <div class="card-post__title"><?php the_title();?></div>
                                            <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                        </div>
                                    </a>
                                </div>
                       
                        <? }
                        } else {
                            // No posts
                        }
                        wp_reset_postdata(); ?>
                    </div>
                    <?php if ( $other_term_posts->max_num_pages > 1 ) { ?>
                        <div class="section-button">
                            <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                            <div id="loader" style="display:none; margin:10px 0;"></div>
                        </div>
                    <? } ?>
                </div>
            </div>
        </section>

<?php



        }
    }

    wp_reset_postdata();

    if (!empty($guide_post_id)) {
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides',
            'p' => $guide_post_id,
        );

        $show_guide = new WP_query ( $args );

            if($show_guide->have_posts()) {

            while ( $show_guide->have_posts() ) :
                $show_guide->the_post();
            ?>

    <section itemscope itemtype="https://schema.org/BlogPosting">
    <div itemprop="mainEntityOfPage" content="<?php echo get_term_link($term_id) ?>"></div>
    <div class="container-post__wrapper container-fluid p-0">
        <div class="container-post container-guide">
            <div class="container-post__inner">
                <div class="post-info__container post-info__container-guide">
                    <div class="post-header__container container-fluid">
                        <div class="post-header__wrapper">
                            <h1 itemprop="headline name" class="pt-md-3 pt-3"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    
                    <div class="post-info__wrapper">
                        <div class="d-none" itemprop="datePublished" content="<?php echo get_the_date('c'); ?>"></div>
                        <div class="d-none" itemprop="dateModified" content="<?php echo get_the_modified_date('c'); ?>"></div>
                        <div class="d-lg-flex <?php if ((get_the_time('j F Y') !== get_the_modified_date('j F Y')) && $checkbox_modified['show_modified'][0] !== 'no') { echo 'd-sm-none d-none'; }?> post-info__date">
                            <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/calendar.svg">
                            <div class="post-info__column">
                                <span class="post-info__column-head">Опубликовано:</span>
                                <span class="post-info__column-value"><?php echo get_the_time('j F Y'); ?></span>
                            </div>
                        </div>

                        <?php 

                        $checkbox_modified = get_post_meta( $post->ID );
                        if ((get_the_time('j F Y') !== get_the_modified_date('j F Y')) && $checkbox_modified['show_modified'][0] !== 'no') { ?>

                        <div class="post-info__edit">
                            <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/edit.svg">
                            <div class="post-info__column">
                                <span class="post-info__column-head">Обновлено:</span>
                                <span class="post-info__column-value"><?php echo get_the_modified_date('j F Y'); ?></span>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="post-info__user">
                            <img class="post-info__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/user.svg">
                            <div class="post-info__column">
                                <? $post_authors = wp_get_post_terms(get_the_ID(), 'users', array('fields' => 'names')); 
                                if ( ! empty( $post_authors ) ) {

                                    $count = count($post_authors);
                                    $output = array();

                                    foreach($post_authors as $post_author) {
                                        $output[] = $post_author;
                                        unset($post_author);
                                    }
                                ?>
                                <span class="post-info__column-head"><?php if($count > 1) { echo 'Авторы:'; } else { echo 'Автор:'; }?></span>
                                <span class="post-info__column-value" itemprop="author">

                                    <?php

                                    echo implode("<br>", $output);

                                    }

                                else { wp_set_post_terms( get_the_ID(), 'Эльви Усманова', 'users', true );

                                }

                                ?>
                                </span>
                            </div>
                        </div>
                    </div> 
                </div>
                
                <div class="post-featured-img__wrapper" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                    <div class="d-none" itemprop="url image"><?php echo the_post_thumbnail_url(); ?></div>
                    <div class="post-featured-img__inner"><?php the_post_thumbnail('large'); ?></div>
                </div>
                
                <?php 

                if (!empty( return_lead( get_the_ID() ) ) ) { echo '<p itemprop="description"><strong>' . return_lead( get_the_ID() ) .  '</strong></p>'; }

                $post_lead_hr = get_post_meta( get_the_ID() , 'lead_meta_box_hr');
                if ($post_lead_hr[0] == 'yes') { echo '<hr>'; }

                ?>
                <div itemprop="articleBody">
                <?php the_content(); ?>
                </div>
                <!-- Photo Source -->
                <?php 
                $related_posts = get_field('photo_source', get_the_ID());
                
                if( $related_posts[0] == '1' ) {

                ?>
                <div class="photo-source__container">
                    <div class="photo-source__wrapper">
                        <div class="photo-source__source">
                            <img class="photo-source__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/photo.svg">
                            <div class="photo-source__row">
                                <span class="photo-source__row-head">Фотографии:</span>


                                <?php

                                    $photo_source_values = get_field('photo_source_value');
                                      if( $photo_source_values ) { ?>
                                          <span class="photo-source__row-value">
                                              <?php

                                                foreach( $photo_source_values as $photo_source_value ) {

                                                    if ($photo_source_value == 'author') {


                                                        $photo__author_values = get_field('photo__author-value');
                                                        $authors = [];

                                                        foreach( $photo__author_values as $photo__author_value ) {


                                                            $author = get_term($photo__author_value, 'users');
                                                            $author_name = $author->name;
                                                            //echo $author_name;
                                                            $authors[] = $author_name;
                                                        }

                                                    }

                                                    else if ($photo_source_value == 'site') {

                                                        $sites = [];

                                                        while( have_rows('photo__site-repeater') ) : the_row();

                                                                // Get parent value.
                                                                $photo__site_group = get_sub_field('photo__site-group');


                                                                // Loop over sub repeater rows.
                                                                if( have_rows('photo__site-group')) {
                                                                    while( have_rows('photo__site-group') ) : the_row();

                                                                        // Get sub value.
                                                                        $photo__site_title = get_sub_field('photo__site-title');
                                                                        $photo__site_url = get_sub_field('photo__site-url');

                                                                        if(!empty($photo__site_url)) {

                                                                        $sites[] = '<a href="'.$photo__site_url.'">'.$photo__site_title.'</a>';

                                                                        }

                                                                        else {

                                                                        $sites[] = $photo__site_title;

                                                                        }

                                                                    endwhile;
                                                                }
                                                            endwhile;
                                                    }

                                                    else if ($photo_source_value == 'photographer') {

                                                        $photographers = [];

                                                        while( have_rows('photo__photographer-repeater') ) : the_row();

                                                                // Get parent value.
                                                                $photo__photographer_group = get_sub_field('photo__photographer-group');


                                                                // Loop over sub repeater rows.
                                                                if( have_rows('photo__photographer-group')) {
                                                                    while( have_rows('photo__photographer-group') ) : the_row();

                                                                        // Get sub value.
                                                                        $photo__photographer_name = get_sub_field('photo__photographer-name');
                                                                        $photo__photographer_url = get_sub_field('photo__photographer-url');

                                                                        if(!empty($photo__photographer_url)) {

                                                                        $photographers[] = '<a href="'.$photo__photographer_url.'">'.$photo__photographer_name.'</a>';

                                                                        }

                                                                        else {

                                                                        $photographers[] = $photo__photographer_name;

                                                                        }

                                                                    endwhile;
                                                                }
                                                            endwhile;
                                                    }



                                                }
                                            } 
                                            $sources = array_merge((array)$authors, (array)$photographers, (array)$sites);
                                            $sources = implode(', ', $sources);
                                            echo $sources;

                                            ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php  } else { echo ''; } ?>

                <!-- Tags -->
                <div class="post-tags__container">
                    <div class="post-tags__wrapper">
                        <?php if(!empty(get_the_tag_list())) { ?><div class="d-none" itemprop="keywords" content="<?php echo implode( ', ', array_map( function( $tag ){ return $tag->name;}, get_the_tags() ) );?>"></div><?php }?>
                    <?php echo get_the_tag_list();?>
                    </div>
                </div>

                <!-- Related Posts -->
                <?php 
                $related_posts = get_field('related-posts');
                $related_posts__ids = get_field('related-posts__ids');

                if ($related_posts[0] == '1') {
                    // check if array has values
                    if ($related_posts__ids !== []) {

                ?>
                <div class="related-posts__container">
                    <div class="related-posts__wrapper">
                        <div class="related-posts__heading">
                            <!-- <img class="related-posts__icon" src="<?php //echo get_template_directory_uri(); ?>/assets/img/book.svg"> -->
                            <div class="related-posts__column">
                                <span class="related-posts__column-head">Смотрите также:</span>
                            </div>
                        </div>

                        <?php $related_posts__ids = get_field('related-posts__ids'); ?>
                        <div class="related-posts__list-wrapper">
                            <ul>
                            <?php
                            foreach ($related_posts__ids as $related_posts__id) {
                            ?>
                                <li><a href="<?php echo get_the_permalink($related_posts__id);?>"><?php echo get_the_title($related_posts__id);?></a></li>
                            <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php  } }?>
                
                <div class="d-none" itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
                <link class="d-none" itemprop="sameAs" href="<?php echo get_home_url(); ?>" />
                <div class="d-none" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject"> 
                <link class="d-none" itemprop="url image" href="<?php echo get_template_directory_uri() .'/assets/img/rv_snippet.png'?>"/>
                </div> 
                <div class="d-none" itemprop="name" content="<?php bloginfo('name');?>"></div>
                <div class="d-none" itemprop="address" content="<?php bloginfo('name');?>"></div>
                <div class="d-none" itemprop="telephone" content="<?php echo get_home_url(); ?>"></div>
                </div>
                
                <div class="social-buttons__container">
                <!------ Rambler.Likes script start ------>
                <div class="rambler-share"></div>
                    <script>
                    (function() {
                    var init = function() {
                    RamblerShare.init('.rambler-share', {
                        "utm": "utm_medium=social",
                        "counters": true,
                        "buttons": [
                            "vkontakte",
                            "facebook",
                            "odnoklassniki",
                            "telegram",
                            "viber",
                            "whatsapp"
                        ]
                    });
                    };
                    var script = document.createElement('script');
                    script.onload = init;
                    script.async = true;
                    script.src = 'https://developers.rambler.ru/likes/v1/widget.js';
                    document.head.appendChild(script);
                    })();
                    </script>
                    <!------   Rambler.Likes script end  ------>

                    </div>
                    <div class="comments__container">
                    <div class="comments__heading">Комментарии:</div>
                    <!------ Cackle comments start------>
                        <div id="mc-container"></div>
                        <script type="text/javascript">
                        cackle_widget = window.cackle_widget || [];
                        cackle_widget.push({widget: 'Comment', id: 76698, channel: '<?php $url = get_permalink(); $url = str_replace( home_url(), '', $url ); echo $url; ?>'});
                        (function() {
                            var mc = document.createElement('script');
                            mc.type = 'text/javascript';
                            mc.async = true;
                            mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                        })();
                        </script>
                        <a id="mc-link" href="http://cackle.me">Комментарии для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>
                        <!------ Cackle comments end------>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
                
    endwhile;
                
    }
        
    wp_reset_postdata();
        
    ?>
        
    <!-- Related Posts -->

    <?php
        
    if ($term_location_type == 'city') { 
        
    $get_country_posts = wp_get_term_taxonomy_parent_id($term_id, 'location'); 
    
    }
    else { $get_country_posts = $term_id; }
        
    $country_term_posts = get_the_terms( $get_country_posts , 'location' );
    $country_term_posts_args = array(
    'post_type'   => 'post',
    'posts_per_page' => '-1',
        'tax_query' => array(
                array(
                'taxonomy' => 'location',
                'field' => 'term_id',
                'terms' => $get_country_posts,
                 ),
            ),
    );
    $country_term_posts = count( get_posts( $country_term_posts_args ) );
    $min_posts = '6';

    if ( $country_term_posts >= $min_posts  ) {

    $args = array(
        'post_type'   => 'post',
        'post__not_in' => array($guide_post_id),
        'posts_per_page' => '6',
        'orderby' => 'rand',
            'tax_query' => array(
                array(
                'taxonomy' => 'location',
                'field' => 'term_id',
                'terms' => $get_country_posts,
                'include_children' => 'false',
                 ),
            ),
        );
        $more_term_posts = new WP_query ( $args );
        
     }
        
    else {
        
        $args = array(
        'post_type'   => 'post',
        'category__not_in' => $term_id,
        'posts_per_page' => '6',
        'orderby' => 'rand',
        
        );
        $more_term_posts = new WP_query ( $args );
        
    }
    ?>
    <section class="mt-3">
        <div class="section-wrapper container-post">
            <div class="section-header">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/furs.svg" width="60" height="40">
                <h2>Что еще почитать:</h2>
            </div>
        <div class="section-content fade">
            <?php echo get_guide_terms()?>
            <div class="container">
                <div class="row no-gutters">
                    <?php if ( $more_term_posts->have_posts() ) {
                    while ( $more_term_posts->have_posts() ) {
                            $more_term_posts->the_post(); ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-post__wrapper fade">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php echo wp_trim_words( get_the_content(), 55, '&hellip;' );?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? }
                    } else {
                        // No posts
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            </div>
        </div>
    </section>
        
<?php    } // end Guides ?>

<?php 
get_footer(); 
?>