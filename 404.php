<?php
/**
 * Template Name: 404
 */
get_header();
?>

    <section class="h-100">
        <div class="section-wrapper bg-white container-fluid d-flex justify-content-center align-items-center mb-n5 h-100 fade">
            <div class="section-header-front section-header-404">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mountains.svg" width="250" height="80">
                <h1>404</h1>
                <p>Запрашиваемая вами страница не найдена, сожалеем.<br>Попробуйте воспользоваться <a href="<?php echo get_home_url(); ?>/?s">поиском</a>.</p>
            </div>
        </div>
    </section>

<?php 
get_footer(); 
?>