<?php
/**
 * Template Name: V
 */

$args = array(
'posts_per_page' => 10,
'orderby'     => 'modified',    
);
query_posts( $args );
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        <?php do_action('rss2_ns'); ?>>
<channel>
        <title><?php bloginfo_rss('name'); ?></title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description') ?></description>
        <language>ru</language>
        <generator>RSS</generator>
        <?php while(have_posts()) : the_post(); ?>
                <item>
                        <title><?php the_title(); ?></title>
                        <link><?php the_permalink(); ?></link>
                        <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                        <dc:creator>РУКИ-В-БОКИ</dc:creator>
                        <guid isPermaLink="false"><?php the_guid(); ?></guid>
                        <enclosure url="<?php echo get_the_post_thumbnail_url();?>" type="image/jpeg"/>
                        <description><![CDATA[<?php 

                        $id = get_the_ID();
                        if (!empty( return_lead( $id ) )) { 
                                    echo return_lead( $id ); 
                                } else { 
                                    echo wp_trim_words( get_the_content(), 55, '&hellip;' ); 
                                }
                        ?>]]>

                        </description>
                        <?php rss_enclosure(); ?>
                        <?php do_action('rss2_item'); ?>
                </item>
        <?php endwhile; ?>
</channel>
</rss>