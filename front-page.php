<?php
/**
 * Template Name: Главная страница
 */
get_header();
?>

    <?php

    // Guides

    $guide_ids = return_guide_ids();
    $args = array(
        'post_type'   => 'post',
        'category_name' => 'guides',
        'posts_per_page' => '-1',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'location',
                'field'    => 'term_id',
                'terms'    => $guide_ids,
                ),
            ),
        );

        $guides = new WP_query ( $args );
        $post_ids = wp_list_pluck( $guides->posts, 'ID' );
    ?>
    <section class="front-section">
        <div class="section-wrapper bg-grey container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/guide.svg" width="50" height="50">
            <h2>Мои путеводители</h2>
            </div>
            <div class="section-content">
                <div class="container">        
                    <div class="swiper-container swiper_guide card-container">
                        <div class="swiper-wrapper" >
                <?php
                foreach ($post_ids as $post_id) {
                    
                $terms =  wp_get_object_terms($post_id,'location'); 
                $deep_terms = get_deep_child_terms($terms);
                
                    foreach ($deep_terms as $deep_term) {
                        $term_id = $deep_term->term_id;
                        $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                        if ($check_type == 'city') {
                            $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                            $guide_flag = get_term_meta( $parent_id, 'flag-url', true );
                            $guide_title = $deep_term->name;
                            $guide_url = get_term_link($term_id);
                            $guide_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );

                        }
                        else if ($check_type == 'country') {
                                $guide_flag = get_term_meta( $term_id, 'flag-url', true );
                                $guide_title = $deep_term->name;
                                $guide_url = get_term_link($term_id);
                                $guide_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );
                        }
                ?>
                            <div class="swiper-slide">
                                <div class="card-wrapper-main">
                                    <div class="card-wrapper">
                                        <a href="<?php echo $guide_url;?>">
                                            <div class="card-image" style="background-image:url('<?php echo $guide_img; ?>')">
                                            <div class="card-flag text-right"><img src="<?php echo $guide_flag; ?>" width="35" height="25"></div>
                                            <div class="card-title"><?php echo $guide_title; ?></div>

                                            <div class="card-category-wrapper">

                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/icons/location.svg" width="13" height="21">
                                            </div>

                                            <div class="card-category">Путеводитель</div>
                                            </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
            <?php 
                    } 
                } 
                wp_reset_postdata();     
            ?>
        
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
            
            <div class="section-button">
                <a href="<?php echo get_home_url(); ?>/guides/"><div class="button-show-cat"><img src="<?php echo get_template_directory_uri();?>/assets/icons/location-white.svg" width="10" height="10"><span>Все путеводители</span><img src="<?php echo get_template_directory_uri();?>/assets/icons/arrow-right.svg" width="7" height="7"></div></a>
            </div>
        </div>
    </section>

    <?php

    // Latest Posts
    /*
    $guide_cat = get_category_by_slug('guides');
    $innovation_cat = get_category_by_slug('innovation');
    $notes_cat = get_category_by_slug('advice');
    $recipe_cat = get_category_by_slug('recipe');

    $cat_not_in = array ($guide_cat->term_id, $innovation_cat->term_id, $notes_cat->term_id, $recipe_cat->term_id);
    $cat_not_in = implode(', ', $cat_not_in);
    
    */

    $args = array(
        'post_type'   => 'post',
        //'category__not_in' => array(3, 4, 6, 357),
        'category__not_in' => array(357),
        'posts_per_page' => '6',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => 1,
        );

    $latest_posts = new WP_query ( $args );

    ?>

    <section class="front-section">
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/furs.svg" width="60" height="40">
                <h2>Новое на сайте</h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div id="posts" class="row no-gutters" data-max-page="<?php echo $latest_posts->max_num_pages;?>" data-page-type="latest">
                    <?php if ( $latest_posts->have_posts() ) {
                    while ( $latest_posts->have_posts() ) {
                            $latest_posts->the_post(); ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-post__wrapper fade">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? }
                    } else {
                        // No posts
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if ( $latest_posts->max_num_pages > 1 ) { ?>
                <div class="section-button">
                    <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                    <div id="loader" style="display:none; margin:10px 0;"></div>
                </div>
            <? } ?>
        </div>
    </section>

    <?php

    // Countries

    $args = array(
        'post_type'   => 'post',
        'posts_per_page' => '50',
        'tax_query' => array (
                    'taxonomy' => 'location',
                    'operator' => 'EXISTS',
            ),
        );

    $countries = new WP_query ( $args );
    $countries_post_ids = wp_list_pluck( $countries->posts, 'ID' ); 
    $country_ids = [];

            foreach ($countries_post_ids as $countries_post_id) {
                
            $country_terms =  wp_get_object_terms( $countries_post_id, 'location' ); 
            $country_deep_terms = get_deep_child_terms( $country_terms );


                foreach ($country_deep_terms as $country_deep_term) {

                    $check_type_country = get_term_meta( $country_deep_term->term_id, 'location-type', true );

                    if ($check_type_country == 'country') {
                        $country_ids[] .= $country_deep_term->term_id;
                    }
                    
                }
                
            }

            $country_ids = array_unique($country_ids); // Remove dublicates
            $country_ids = array_slice($country_ids, 0, 8); // Get first 8 values

            ?>

    <section class="front-section">
        <div class="section-wrapper bg-grey container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/plane.svg" width="50" height="50">
            <h2>Популярные страны</h2>
            </div>
            <div class="section-content fade">
                <div class="container">        
                    <div class="swiper-container swiper_country card-container">
                        <div class="swiper-wrapper" >   
                    
                        <?php foreach($country_ids as $country_id) {

                        $country_img_id = get_term_meta( $country_id, '_thumbnail_id', 1 );
                        $country_img = wp_get_attachment_image_url( $country_img_id, 'medium_large' );
                        $country_flag = get_term_meta( $country_id, 'flag-url', true );
                        $country_title = get_term( $country_id )->name;
                        $country_url = get_term_link( (int)$country_id, 'location' );

                        ?>

                            <div class="swiper-slide">
                                <div class="card-wrapper-main">
                                    <div class="card-wrapper">
                                        <a href="<?php echo $country_url; ?>">                    
                                            <div class="card-country-image" style="background-image:url('<?php echo $country_img; ?>')">
                                            <div class="card-country-title"><?php echo $country_title; ?></div>
                                            <div class="card-country-flag"><img src="<?php echo $country_flag; ?>" width="45" height="35"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                             </div>
    
                        <?php }?>
                            
                        </div>
                       
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    
                    </div>
                </div>
                <div class="section-button">
                    <a href="<?php echo get_home_url();?>/travel/"><div class="button-show-cat" style="padding: 12px 20px;"><img src="<?php echo get_template_directory_uri();?>/assets/icons/globe.svg" width="20" height="20"><span>Все страны</span>
                    <img src="<?php echo get_template_directory_uri();?>/assets/icons/arrow-right.svg" width="7" height="7"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <?php

    // Advice

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'advice',
        'posts_per_page' => '4',
        'orderby' => 'rand', 
        );

    $advices = new WP_query ( $args );

    ?>

    <section class="front-section">
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/suitcase.svg" width="50" height="50">
                <h2>Подготовка к поездке</h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div class="row no-gutters">
                    <?php if ( $advices->have_posts() ) {
                    while ( $advices->have_posts() ) {
                            $advices->the_post(); ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php the_permalink()?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-notes-image" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover">
                                    <div class="card-notes-title"><?php the_title();?></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? }
                    } else {
                        // No posts
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <div class="section-button">
                <a href="<?php echo get_home_url();?>/advice/">
                <div class="button-show-cat"><img src="<?php echo get_template_directory_uri();?>/assets/icons/advice.svg" width="15" height="15"><span>Все советы</span><img src="<?php echo get_template_directory_uri();?>/assets/icons/arrow-right.svg" width="7" height="7"></div>
                </a>
            </div>
        </div>
    </section>

    <?php

    // Innovation

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'innovation',
        'posts_per_page' => '3',
        'orderby' => 'rand', 
        );

        $innovations = new WP_query ( $args );
        $post_ids = wp_list_pluck( $innovations->posts, 'ID' );
    ?>
    <section class="front-section">
        <div class="section-wrapper bg-grey container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gear.svg" width="50" height="50">
            <h2>Инновации</h2>
            </div>
            <div class="section-content fade">
                <div class="container">
                    <div class="row no-gutters">
                        
                    <?php
                    foreach ($post_ids as $post_id) {

                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                
                    foreach ($deep_terms as $deep_term) {
                        $term_id = $deep_term->term_id;
                        $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                        if ($check_type == 'city') {
                            $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                            $innovasion_flag = get_term_meta( $parent_id, 'flag-url', true );
                            $innovasion_title = $deep_term->name;
                            $innovasion_url = get_permalink($post_id);
                            $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );

                        }
                        else if ($check_type == 'country') {
                                $innovasion_flag = get_term_meta( $term_id, 'flag-url', true );
                                $innovasion_title = $deep_term->name;
                                $innovasion_url = get_permalink($post_id);
                                $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );
                        }
                    ?>
                       <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <a href="<?php echo $innovasion_url;?>">
                                        <div class="card-text-image" style="background-image:url('<?php echo $innovasion_img; ?>')">
                                        <div class="card-flag text-right"><img src="<?php echo $innovasion_flag; ?>" width="35" height="25"></div>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gear-bold.svg" width="13" height="21"></div>
                                            <div class="card-category">
                                            <?php echo $innovasion_title; ?></div>
                                        </div>
                                        </div>
                                        <div class="card-text-title"><?php echo get_the_title($post_id);?></div>
                                        <div class="card-text-excerpt"><?php if (!empty( return_lead( $post_id ) ) ) { echo return_lead( $post_id ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <?php 
                                } 
                            } wp_reset_postdata();     
                        ?>
                        
                    </div>
                </div>
                <div class="section-button">
                    <a href="<?php echo get_home_url();?>/innovation/">
                    <div class="button-show-cat"><img src="<?php echo get_template_directory_uri();?>/assets/icons/gear-white.svg" width="15" height="15"><span>Все инновации</span><img src="<?php echo get_template_directory_uri();?>/assets/icons/arrow-right.svg" width="7" height="7"></div>
                     </a>
                </div>
            </div>
        </div>
    </section>

    <?php

    // Recipe

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'recipe',
        'posts_per_page' => '4',
        'orderby' => 'rand',
        );

    $recipes = new WP_query ( $args );
    $post_ids = wp_list_pluck( $recipes->posts, 'ID' );
    ?>

    <section class="front-section">
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/cooking.svg" width="40" height="40">
                <h2>Рецепты</h2>
            </div>
        </div>
        <div class="section-content fade">
            <div class="container">
                <div class="row no-gutters">
                    
                    <?php
                    
                    foreach ($post_ids as $post_id) {
                         
                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                        
                        foreach ($deep_terms as $deep_term) {
                    
                            $term_id = $deep_term->term_id;
                            $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                            if ($check_type == 'city') {
                                $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                                $country_flag = get_term_meta( $parent_id, 'flag-url', true );
                                $title = $deep_term->name;

                            }
                            else if ($check_type == 'country') {
                                $country_flag = get_term_meta( $term_id, 'flag-url', true );
                                $title = $deep_term->name;
                            }
                        }
                        
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <a href="<?php echo get_the_permalink($post_id)?>">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <div class="card-image" style="background: url('<?php echo get_the_post_thumbnail_url(($post_id) , 'medium_large'); ?>') no-repeat center center / cover">
                                        <div class="card-recipe-title"><?php echo get_the_title($post_id);?></div>
                                            <?php if(!empty($country_flag)) {?><div class="card-flag text-right"><img src="<?php echo $country_flag;?>" width="35" height="25"></div><?php } ?>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/icons/cooking-bold.svg" width="13" height="21"></div>
                                            <?php if(!empty($title)) {?><div class="card-category">
                                            <?php echo $title;?></div><?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? 
                    }
                       
                    wp_reset_postdata(); ?>
                </div>
            </div>
           
            <div class="section-button">
                <a href="<?php echo get_home_url();?>/recipe/">
                <div class="button-show-cat"><img src="<?php echo get_template_directory_uri();?>/assets/icons/cooking-white.svg" width="15" height="15"><span>Все рецепты</span><img src="<?php echo get_template_directory_uri();?>/assets/icons/arrow-right.svg" width="7" height="7"></div>
                </a>
            </div>
            
        </div>
    </section>
    <section>
        <div class="section-content section-about mb-n5">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-lg-3 col-12 d-flex justify-content-lg-end justify-content-md-center justify-content-center fade">
                        <div class="section-about_img">
                            <a href="<?php echo get_site_url();?>/contacts/">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about.jpg" width="200" height="200">
                            </a>
                        </div>
                    </div>    
                    <div class="col-lg-9 col-12 d-flex d-flex justify-content-end fade">
                        <div class="section-about_text">
                            <h1>Привет!</h1>
                            <p>Меня зовут Эльви, я создатель и редактор сайта «Руки в боки». Я люблю путешествовать по разным уголкам нашей планеты и чаще всего — без турфирм. Я сама нахожу бюджетные самолеты, заказываю гостиницы, прорабатываю маршрут. Многие интересные места подсказывают друзья, которые живут  в разных странах мира. Я также пишу о полезных нововведениях, которые замечаю в поездках. Этот сайт я делаю не одна — над ним работают мои друзья-путешественники. Надеюсь, что информация, собранная здесь, поможет и вам при планировании поездок.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php 
get_footer(); 
?>