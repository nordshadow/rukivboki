<?php
/**
 * Template Name: Категория Инновации
 */
get_header();
?>

    <?php

    // Innovation

    $args = array(
        'post_type'   => 'post',
        'category_name' => 'innovation',
        'posts_per_page' => '9',
        'orderby' => 'modified', 
        'order' => 'DESC',
        'paged' => 1,
        );

        $innovations = new WP_query ( $args );
        $post_ids = wp_list_pluck( $innovations->posts, 'ID' );
    ?>
    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gear.svg" width="50" height="50">
            <h2>Инновации</h2>
            </div>
            <div class="section-content fade">
                <div class="container">
                    <div id="posts" class="row no-gutters" data-max-page="<?php echo $innovations->max_num_pages;?>" data-cat="<?php echo get_query_var('cat');?>">
                        
                    <?php
                    foreach ($post_ids as $post_id) {

                    $terms =  wp_get_object_terms($post_id,'location'); 
                    $deep_terms = get_deep_child_terms($terms);
                
                    foreach ($deep_terms as $deep_term) {
                        $term_id = $deep_term->term_id;
                        $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                        if ($check_type == 'city') {
                            $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                            $innovasion_flag = get_term_meta( $parent_id, 'flag-url', true );
                            $innovasion_title = $deep_term->name;
                            $innovasion_url = get_permalink($post_id);
                            $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );

                        }
                        else if ($check_type == 'country') {
                                $innovasion_flag = get_term_meta( $term_id, 'flag-url', true );
                                $innovasion_title = $deep_term->name;
                                $innovasion_url = get_permalink($post_id);
                                $innovasion_img = get_the_post_thumbnail_url( $post_id , 'medium_large' );
                        }
                    ?>
                       <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="card-wrapper-main fade">
                                <div class="card-wrapper">
                                    <a href="<?php echo $innovasion_url;?>">
                                        <div class="card-text-image" style="background-image:url('<?php echo $innovasion_img; ?>')">
                                        <div class="card-flag text-right"><img src="<?php echo $innovasion_flag; ?>" width="35" height="25"></div>
                                        <div class="card-category-wrapper">
                                            <div class="card-icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gear-bold.svg" width="13" height="21"></div>
                                            <div class="card-category">
                                            <?php echo $innovasion_title; ?></div>
                                        </div>
                                        </div>
                                        <div class="card-text-title"><?php echo get_the_title($post_id);?></div>
                                          <div class="card-text-excerpt"><?php if (!empty( return_lead( $post_id ) ) ) { echo return_lead( $post_id ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <?php 
                                } 
                            } wp_reset_postdata();     
                        ?>
                        
                    </div>
                </div>
                
                <?php if (  $innovations->max_num_pages > 1 ) { ?>
                <div class="section-button">
                    <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                    <div id="loader" style="display:none; margin:10px 0;"></div>
                </div>
                <? } ?>
            </div>
        </div>
    </section>

<?php 
get_footer(); 
?>