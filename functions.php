<?php
/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support( 'title-tag' );
    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
		'header_menu' => 'Header Menu',
		'footer_menu' => 'Footer Menu'
	]);
    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    //add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
    
    add_theme_support( 'editor-style' );
}, 20);

// Enqueue Scripts & Styles 
require get_template_directory() . '/functions/enqueues.php';
// Clean unwated stuff
require get_template_directory() . '/functions/cleanup.php';
// Taxonomy thumbnail
require get_template_directory() . '/functions/taxonomy-thumbnails.php';
// AJAX Load more
require get_template_directory() . '/functions/ajax.php';

// Code after </body> support
if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}
add_action( 'wp_body_open', 'ws_body_open_func', 10 );
function ws_body_open_func() {
// Some scripts code, etc
?>
<?php
}

add_image_size( 'mini', 220, 150, true );
add_filter( 'image_size_names_choose', 'my_custom_sizes' );
function my_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'mini' => 'Мини',
	) );
}

// Thumbnail quality
add_filter( 'jpeg_quality', create_function( '', 'return 90;' ) );

// Disable Gutenberg editor.
add_filter('use_block_editor_for_post_type', '__return_false', 10);
// Don't load Gutenberg-related stylesheets.
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );
function remove_block_css() {
    wp_dequeue_style( 'wp-block-library' ); // Wordpress core
    wp_dequeue_style( 'wp-block-editor' ); // Blocks
    wp_dequeue_style( 'wp-block-components' ); // Blocks
    wp_dequeue_style( 'wp-block-library-theme' ); // Wordpress core
    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
}

// Disable WP comments

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    //if ($pagenow === 'edit-comments.php') {
    //    wp_redirect(admin_url());
    //    exit;
    // }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
//add_action('admin_menu', function () {
//    remove_menu_page('edit-comments.php');
//});

// Remove comments links from admin bar
add_action('admin_init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

// Register Location taxonomy

function register_taxonomy_location() {

	/**
	 * Taxonomy: Location
	 */

	$labels = [
		"name" => __( "Локации", "custom-post-type-ui" ),
		"singular_name" => __( "Локация", "custom-post-type-ui" ),
		"menu_name" => __( "Локации", "custom-post-type-ui" ),
		"all_items" => __( "Все локации", "custom-post-type-ui" ),
		"edit_item" => __( "Редактировать локацию", "custom-post-type-ui" ),
		"view_item" => __( "Посмотреть локацию", "custom-post-type-ui" ),
		"update_item" => __( "Изменить название локации", "custom-post-type-ui" ),
		"add_new_item" => __( "Добавить локацию", "custom-post-type-ui" ),
		"new_item_name" => __( "Название локации", "custom-post-type-ui" ),
		"parent_item" => __( "Родительская локация", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Родительская локация", "custom-post-type-ui" ),
		"search_items" => __( "Поиск локации", "custom-post-type-ui" ),
		"popular_items" => __( "Популярные локации", "custom-post-type-ui" ),
		"separate_items_with_commas" => __( "Разделите локации запятыми", "custom-post-type-ui" ),
		"add_or_remove_items" => __( "Добавить или удалить локацию", "custom-post-type-ui" ),
		"choose_from_most_used" => __( "Выбрать из часто изпользуемых", "custom-post-type-ui" ),
		"not_found" => __( "Локация не найдена", "custom-post-type-ui" ),
		"no_terms" => __( "Нет локаций", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Локации", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'travel', 'with_front' => true, "hierarchical" => true],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "location",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
        "capabilities" => array( 'manage_options', 'edit_posts' ),
			];
	register_taxonomy( "location", [ "post" ], $args );
}
add_action( 'init', 'register_taxonomy_location' );

// Disable + Add new category/location on edit page
function remove_add_buttons() {
  global $pagenow;
  if(is_admin()){   
    echo '<style type="text/css">#category-add-toggle{display: none;}#location-add-toggle{display: none;}</style>';
  }
}
add_action('admin_head','remove_add_buttons');


// Register Users taxonomy


function register_taxonomy_users() {

	/**
	 * Taxonomy: Users
	 */

	$labels = [
		"name" => __( "Авторы", "custom-post-type-ui" ),
		"singular_name" => __( "Автор", "custom-post-type-ui" ),
		"menu_name" => __( "Авторы", "custom-post-type-ui" ),
		"all_items" => __( "Все авторы", "custom-post-type-ui" ),
		"edit_item" => __( "Редактировать автора", "custom-post-type-ui" ),
		"view_item" => __( "Посмотреть автора", "custom-post-type-ui" ),
		"update_item" => __( "Обновить автора", "custom-post-type-ui" ),
		"add_new_item" => __( "Добавить нового автора", "custom-post-type-ui" ),
		"new_item_name" => __( "New Автор name", "custom-post-type-ui" ),
		"parent_item" => __( "", "custom-post-type-ui" ),
		"parent_item_colon" => __( "", "custom-post-type-ui" ),
		"search_items" => __( "Поиск автора", "custom-post-type-ui" ),
		"popular_items" => __( "Популярные авторы", "custom-post-type-ui" ),
		"separate_items_with_commas" => __( "Разделяйте авторов запятыми", "custom-post-type-ui" ),
		"add_or_remove_items" => __( "Добавить или удалить авторов", "custom-post-type-ui" ),
		"choose_from_most_used" => __( "Выберите из часто используемых авторов", "custom-post-type-ui" ),
		"not_found" => __( "Автор не найден", "custom-post-type-ui" ),
		"no_terms" => __( "Нет авторов", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Навигация по списку авторов", "custom-post-type-ui" ),
		"items_list" => __( "Список авторов", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Авторы", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => false,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'users', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "users",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
        "capabilities" => array( 'manage_options', 'edit_posts' ),
			];
	register_taxonomy( "users", [ "post" ], $args );
}
add_action( 'init', 'register_taxonomy_users' );


// Add location taxonomy to admin menu

add_action( 'admin_menu', 'add_location_admin_menu_item' );

function add_location_admin_menu_item(){
    
	$taxname = 'location';

	$is_skills = isset($_GET['taxonomy']) && $_GET['taxonomy'] === $taxname;

	if( $is_skills ) add_filter( 'parent_file', '__return_false' );

	$menu_title = 'Локации';
	add_menu_page( 'Локации', $menu_title, 'edit_posts', "edit-tags.php?taxonomy=$taxname", null, 'dashicons-location', 9 );

	$menu_item = & $GLOBALS['menu'][ key(wp_list_filter( $GLOBALS['menu'], [$menu_title] )) ];
	foreach( $menu_item as & $val ){
		
		if( false !== strpos($val, 'menu-top') )
			$val = 'menu-top'. ( $is_skills ? ' current' : '' );

		$val = preg_replace('~toplevel_page[^ ]+~', "toplevel_page_$taxname", $val );
	}

}


// Add Users taxonomy to admin menu

add_action( 'admin_menu', 'add_users_admin_menu_item' );

function add_users_admin_menu_item(){
    
	$taxname = 'users';

	$is_skills = isset($_GET['taxonomy']) && $_GET['taxonomy'] === $taxname;

	if( $is_skills ) add_filter( 'parent_file', '__return_false' );

	$menu_title = 'Авторы';
	add_menu_page( 'Авторы', $menu_title, 'edit_posts', "edit-tags.php?taxonomy=$taxname", null, 'dashicons-admin-users', 9 );

	$menu_item = & $GLOBALS['menu'][ key(wp_list_filter( $GLOBALS['menu'], [$menu_title] )) ];
	foreach( $menu_item as & $val ){
		
		if( false !== strpos($val, 'menu-top') )
			$val = 'menu-top'. ( $is_skills ? ' current' : '' );

		$val = preg_replace('~toplevel_page[^ ]+~', "toplevel_page_$taxname", $val );
	}

}

// Add Guide posts to admin menu
add_action('admin_menu', 'add_guides_admin_menu_item');

function add_guides_admin_menu_item() {
    
    $taxname = 'guides';

	$is_skills = isset($_GET['category_name']) && $_GET['category_name'] === $taxname;

	if( $is_skills ) add_filter( 'parent_file', '__return_false' );

	$menu_title = 'Путеводители';
	add_menu_page( 'Путеводители', $menu_title, 'edit_posts', "edit.php?category_name=guides", null, 'dashicons-admin-site-alt3', 9 );

	$menu_item = & $GLOBALS['menu'][ key(wp_list_filter( $GLOBALS['menu'], [$menu_title] )) ];
	foreach( $menu_item as & $val ){
		
		if( false !== strpos($val, 'menu-top') )
			$val = 'menu-top'. ( $is_skills ? ' current' : '' );

		$val = preg_replace('~toplevel_page[^ ]+~', "toplevel_page_$taxname", $val );
	}
}

	
function edit_location_fields() {
    
    $term_id = $_GET['tag_ID'];
    $term = get_term($term_id, 'location');
    $term_meta = get_term_meta( $term_id, 'location-type', true );
    
    ?>
	<tr class="form-field term-location-type">
	<th><label>Тип локации</label></th>
	<td>
	    <fieldset>
    	<label for="location-country"><input type="radio" id="location-country" name="location-type" value="country" <?php if ($term_meta == 'country') { echo 'checked'; }?>>Страна</label><br>
    	<label for="location-city"><input type="radio" id="location-city" name="location-type" value="city" <?php if ($term_meta == 'city') { echo 'checked'; }?>>Город</label><br>
    	<label for="location-region"><input type="radio" id="location-region" name="location-type" value="region" <?php if ($term_meta == 'region') { echo 'checked'; }?>>Регион</label><br>
        </fieldset>
    </td>
	</tr>
	<tr><td><div id="loader"></div></td></tr>
	<?php
}

function add_location_fields(){
	?>
	<div class="form-field">
		<h2>Тип локации</h2>
        <label for="location-country"><input type="radio" id="location-country" name="location-type" value="country" checked>Страна</label>
        <label for="location-city"><input type="radio" id="location-city" name="location-type" value="city">Город</label>
        <label for="location-region"><input type="radio" id="location-region" name="location-type" value="region" >Регион</label>
	</div>

    <div id="result"></div>
    <div id="loader"></div>
	<?php
}
add_action( 'location_add_form_fields', 'add_location_fields' );
add_action( 'location_edit_form_fields', 'edit_location_fields' );

add_action( 'created_location', 'save_location_fields' );
add_action( 'edited_location', 'save_location_fields' );
 
function save_location_fields( $term_id ) {
	update_term_meta( $term_id, 'location-type', $_POST['location-type']);
	update_term_meta( $term_id, 'flag-url', $_POST['flag-url']);
}

// Location taxonomy columns admin

function location_taxonomy_columns( $columns )
{
	$columns['country-flag'] = __('Флаг');
    $columns['location-type'] = __('Тип локации');
	return $columns;
}
add_filter('manage_edit-location_columns' , 'location_taxonomy_columns');

function location_taxonomy_columns_content( $content, $column_name, $term_id )
{
    if ( 'location-type' == $column_name ) {
        $term_meta = get_term_meta( $term_id, 'location-type', true );
        if ($term_meta == 'region') { $term_meta = 'Регион'; }
        if ($term_meta == 'country') { $term_meta = 'Страна'; }
        if ($term_meta == 'city') { $term_meta = 'Город'; }
        echo "<strong>".$term_meta."</strong>";
    }
    
    if ( 'country-flag' == $column_name ) {
        $flag = get_term_meta( $term_id, 'flag-url', true );
        if(!empty($flag)) { echo '<img src="' . esc_attr( $flag ). '" class="country-flag">'; }
    }
	
}
add_filter( 'manage_location_custom_column', 'location_taxonomy_columns_content', 10, 3 );


// Ajax script to display child locations

add_action('admin_print_footer_scripts', 'my_action_javascript', 99);

function my_action_javascript() {
    ?>
    <script type="text/javascript"> 
        
    jQuery(document).ready(function($) {
        
        var val = $("input[name='location-type']:checked").val();
        var page = '<?php $screen = get_current_screen(); echo $screen->base; ?>';
        var location_id = '<?php if(!empty($_GET['tag_ID'])) { echo $_GET['tag_ID']; } ?>';

        var sendButtonValue = function (val, page, location_id) {
            $.ajax({
                beforeSend: function () {
                    if (page == 'term') {
                        $('form#edittag table tr.form-region').remove();
                        $('form#edittag table tr.form-flag').remove();
                        $('form#edittag table tr.form-country').remove();
                    }
                    $('#loader').removeClass('hidden');
                },
                type: "POST",
                url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                data: "action=get_radio_button_vale&page="+page+"&location_id="+location_id+"&radio_val="+val,
                success:function(response){
                    if (page == 'term') {
                        $('form#edittag table tr.term-location-type').last().after(response);
                    }
                    else if (page == 'edit-tags') {
                        $('#result').html(response);
                    }
                },
                complete: function () { 
                $('#loader').addClass('hidden');

                },
            });
        };

        sendButtonValue(val,page,location_id);

        $("input[name='location-type']").change(function(){
            if( $(this).is(":checked") ){
                var val = $(this).val();
                var page = '<?php $screen = get_current_screen(); echo $screen->base; ?>';
            }
            sendButtonValue(val,page,location_id);
        });

    });
        
</script> 
<?php
}

// Ajax call to display child locations

function display_location_dropdown () {

    $term_id = $_POST['location_id'];
    $term = get_term($term_id, 'location');
    $flag = get_term_meta( $term_id, 'flag-url', true );
    
    $args_regions_new = array(
            'hide_empty'    => false,
            'meta_key'      => 'location-type',
            'meta_value'    => 'region',
            'taxonomy'      => 'location',
            'name'          => 'parent',
			'orderby'       => 'name',
        );

    $args_regions_edit = array(
            'hide_empty'    => false,
            'meta_key'      => 'location-type',
            'meta_value'    => 'region',
            'taxonomy'      => 'location',
            'name'          => 'parent',
			'orderby'       => 'name',
			'selected'      => $term->parent,
			'exclude'       => $term_id,
        );

    $args_regions_new = apply_filters( 'taxonomy_parent_dropdown_args', $args_regions_new, 'location', 'new' );
    $args_regions_edit = apply_filters( 'taxonomy_parent_dropdown_args', $args_regions_edit, 'location', 'edit' );
    
    $args_countries_new = array(
            'hide_empty'    => false, // also retrieve terms which are not used yet
            'meta_key'      => 'location-type',
            'meta_value'    => 'country',
            'taxonomy'      => 'location',
            'name'          => 'parent',
			'orderby'       => 'name',
        );

    $args_countries_edit = array(
            'hide_empty'    => false, // also retrieve terms which are not used yet
            'meta_key'      => 'location-type',
            'meta_value'    => 'country',
            'taxonomy'      => 'location',
            'name'          => 'parent',
			'orderby'       => 'name',
            'selected'      => $term->parent,
			'exclude'       => $term_id,
        );

	$args_countries_new = apply_filters( 'taxonomy_parent_dropdown_args', $args_countries_new, 'location', 'new' );
    $args_countries_edit = apply_filters( 'taxonomy_parent_dropdown_args', $args_countries_edit, 'location', 'edit' );

    if($_POST['radio_val'] == 'country') {

        if ($_POST['page'] == 'edit-tags') { echo '<div class="form-field form-region"><h2>Выберите регион</h2>'; wp_dropdown_categories($args_regions_new); echo '</div><div class="form-field"><h2>Флаг страны</h2><input name="flag-url" id="flag-url" type="url" value="'. esc_attr( $flag ) .'" placeholder="https://example.com/Flag_of_Russia.svg" /><p>URL флага страны в формате .svg</p></div>'; }

        else if ($_POST['page'] == 'term') { echo '<tr class="form-field form-region"><th><label>Выберите регион</label></th><td>'; wp_dropdown_categories($args_regions_edit); echo '</td></tr>'; echo '<tr class="form-field form-flag"><th><label>Флаг страны</label></th><td><input name="flag-url" id="flag-url" type="url" value="'. esc_attr( $flag ) .'" placeholder="https://example.com/Flag_of_Russia.svg" /><p>URL флага страны в формате .svg</p></td></tr>'; }?>

    <?php  }

    else if($_POST['radio_val'] == 'city') {

        if ($_POST['page'] == 'edit-tags') { echo '<div class="form-field form-country"><h2>Выберите страну</h2>'; wp_dropdown_categories($args_countries_new); echo '</div>'; }

    	else if ($_POST['page'] == 'term') { echo '<tr class="form-field form-country"><th><label>Выберите страну</label></th><td>'; wp_dropdown_categories($args_countries_edit); echo '</td></tr>';}?> 

	<?php  }

    exit;
}
add_action( 'wp_ajax_get_radio_button_vale', 'display_location_dropdown' );
add_action( 'wp_ajax_nopriv_get_radio_button_vale', 'display_location_dropdown' );


// Megamenu

class BEM_Walker_Nav_Menu extends Walker_Nav_Menu {

  public function start_lvl( &$output, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = str_repeat( $t, $depth );

    // sub menu classes

    $submenu_classes = array( $block . '__sub-menu' );
    $submenu_classes = implode( '  ', array_filter( $submenu_classes ) );

    $output .= "{$n}{$indent}<ul class=\"$submenu_classes\">{$n}";
  }

  public function end_lvl( &$output, $depth = 0, $args = null ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent  = str_repeat( $t, $depth );
    $output .= "$indent</ul>{$n}";
  }

  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

    // list item
    $item_classes = array( $block . '__item' );
    if ( $item->current ) {
      $item_classes[] = $block . '__item--active';
    }
    if ( $item->current_item_ancestor ) {
      $item_classes[] = $block . '__item--active-ancestor';
    }
    if ( $item->current_item_parent ) {
      $item_classes[] = $block . '__item--active-parent';
    }
    $classes = get_post_meta( $item->ID, '_menu_item_classes' );
    foreach ( $classes[0] as $class ) {
      if ( $class ) {
        $item_classes[] = $block . '__item--' . $class;
      }
    }
    if ( is_single() ) {
      $category = get_the_category();
      $cat_id = $category[0]->cat_ID;
      $ancestors = get_ancestors( $cat_id, 'category' );
      if ( in_array( $item->object_id, $ancestors ) ) {
        $item_classes[] = $block . '__item--active-ancestor';
      }
    }
    $item_classes = implode( '  ', array_filter( $item_classes ) );

    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    $output .= $indent . '<li class="' . $item_classes .'">';

    // link attributes
    $atts           = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    if ( '_blank' === $item->target && empty( $item->xfn ) ) {
      $atts['rel'] = 'noopener noreferrer';
    } else {
      $atts['rel'] = $item->xfn;
    }
    $atts['href']         = ! empty( $item->url ) ? $item->url : '';
    $atts['aria-current'] = $item->current ? 'page' : '';

    if ( $item->current ) $atts['href'] = '';

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
        $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
          
        // RV custom links
        if ($item->title == 'Европа') { $attributes .= ' ' . $attr . '="/travel/#europe"'; }
        else if ($item->title == 'Азия') { $attributes .= ' ' . $attr . '="/travel/#asia"'; }
        else if ($item->title == 'Африка') { $attributes .= ' ' . $attr . '="/travel/#africa"'; }
        else if ($item->title == 'Америка') { $attributes .= ' ' . $attr . '="/travel/#america"'; }
        else if ($item->title == 'Океания') { $attributes .= ' ' . $attr . '="/travel/#oceania"'; }
          
        else { $attributes .= ' ' . $attr . '="' . $value . '"'; }
      }
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );
    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    // link
    $item_output = $args->before;
    $item_output .= '<a class="' . $block . '__link"'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  public function end_el( &$output, $item, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $output .= "</li>{$n}";
  }

}

// Custom Walker for all countries submenu

class Walker_Taxonomy_Location extends Walker_Category
    {
        function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        
        $cat_name = apply_filters(
            'list_cats',
            esc_attr( $category->name ),
            $category
        );

        $link = '<a href="' . esc_url( get_term_link( $category ) ) . '" ';
        $link .= '>';
       

        $flag = get_term_meta( $category->cat_ID, 'flag-url', true );
        if(!empty($flag)) { $link .= '<img src="' . esc_attr( $flag ). '" width="40" height="30" class="country-flag" alt="'. $cat_name . '"><span>'. $cat_name . '</span></a>'; }
        else { $link .=  $cat_name . '</a>'; }
        if ( ! empty( $args['show_count'] ) ) {
          $link .= ' (' . number_format_i18n( $category->count ) . ')';
        }
        $output .= "\t<li";
        $class = 'header-nav__link';
        $output .=  ' class="' . $class . '"';
        $output .= ">$link\n";
    }
}

// All countries submenu

add_filter( 'walker_nav_menu_start_el', 'taxonomy_location_sub_menu', 10, 4 );
function taxonomy_location_sub_menu( $item_output, $item, $depth, $args ) {
	if( $item->object === 'location'  ) {
		$sub_cats = wp_list_categories( [ 
            'hide_empty'    => false, // also retrieve terms which are not used yet
            'meta_key'      => 'location-type',
            'meta_value'    => 'country',
            'taxonomy'      => 'location',
            'show_option_none'    => false,
            'style' => 'list',
            'child_of' => $item->object_id, 
            'echo' => 0, 
            'title_li' => '',
            'walker' => new Walker_Taxonomy_Location,
                                
            ] );
        
		if( ! empty( $sub_cats ) ) {
			
            $class = "";
            $item_output .= '<ul class="header-nav__locations">' . "\n";
			$item_output .= $sub_cats;
			$item_output .= '</ul>' . "\n";
            
            }
        }
    
    if( $item->object === 'category' && $item->title === 'Путеводители' ) {
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides', 
            'orderby' => 'modified', 
            'order' => 'DESC',

            );
            $posts = new WP_query ( $args ); 
            $post_ids = wp_list_pluck( $posts->posts, 'ID' );
        
            $item_output .= '<div class="header-nav__chveron-down-wrapper"><div class="header-nav__chveron-down"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></div></div>';
            $item_output .= '<ul class="header-nav__sub-menu">' . "\n";
        
            foreach ($post_ids as $post_id) {
                $terms =  wp_get_object_terms($post_id,'location'); 
                $deep_terms = get_deep_child_terms($terms);
                
                foreach ($deep_terms as $deep_term) {
                    $term_id = $deep_term->term_id;
                    $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );

                    if ($check_type == 'city') {
                        $parent_id = wp_get_term_taxonomy_parent_id($term_id, 'location');
                        $guide_flag = get_term_meta( $parent_id, 'flag-url', true );
                        $guide_title = $deep_term->name;
                        $guide_url = get_term_link($term_id);

                    }
                    else if ($check_type == 'country') {
                            $guide_flag = get_term_meta( $term_id, 'flag-url', true );
                            $guide_title = $deep_term->name;
                            $guide_url = get_term_link($term_id);
                    }
                    $item_output .= '<a href="' . $guide_url .'">';
                    $item_output .= '<ul class="header-nav__guide">' . "\n";
                    if(!empty($guide_flag)) {
                        $item_output .= '<img src="' . esc_attr( $guide_flag ). '" class="country-flag" width="40" height="30">';
                    }
                    
                    $item_output .= $guide_title ;
                    $item_output .= '</ul>' . "\n";
                    $item_output .= '</a>' . "\n";
                }
            }
        
            $item_output .= '</ul>' . "\n";
    }
  return $item_output;
}

add_filter( 'walker_nav_menu_start_el', 'add_chevron_icon_to_locations_menu', 10, 4 );

function add_chevron_icon_to_locations_menu( $item_output, $item, $depth, $args ) {
    if ( $item->object === 'page' && $item->title === 'Все страны' ) {
        return $item_output . '<div class="header-nav__chveron-down-wrapper"><div class="header-nav__chveron-down"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></div></div>';
    }
    else { 
        return $item_output; 
    }
}

/**
 * Automatically set/assign parent taxonomy terms to posts
 * 
 * This function will automatically set parent taxonomy terms whenever terms are set on a post,
 * with the option to configure specific post types, and/or taxonomies.
 *
 *
 * @param int    $object_id  Object ID.
 * @param array  $terms      An array of object terms.
 * @param array  $tt_ids     An array of term taxonomy IDs.
 * @param string $taxonomy   Taxonomy slug.
 * @param bool   $append     Whether to append new terms to the old terms.
 * @param array  $old_tt_ids Old array of term taxonomy IDs.
 */
add_action( 'set_object_terms', 'auto_set_parent_terms', 9999, 6 );
function auto_set_parent_terms( $object_id, $terms, $tt_ids, $taxonomy, $append, $old_tt_ids ) {
	/**
	 * We only want to move forward if there are taxonomies to set
	 */
	if( empty( $tt_ids ) ) return FALSE;
	/**
	 * Set specific post types to only set parents on.  Set $post_types = FALSE to set parents for ALL post types.
	 */
	$post_types = array( 'post' );
	if( $post_types !== FALSE && ! in_array( get_post_type( $object_id ), $post_types ) ) return FALSE;
	/**
	 * Set specific post types to only set parents on.  Set $post_types = FALSE to set parents for ALL post types.
	 */
	$tax_types = array( 'location' );
	if( $tax_types !== FALSE && ! in_array( $taxonomy, $tax_types ) ) return FALSE;
	
	foreach( $tt_ids as $tt_id ) {
		$parent = wp_get_term_taxonomy_parent_id( $tt_id, $taxonomy );
		if( $parent ) {
			wp_set_post_terms( $object_id, array($parent), $taxonomy, TRUE );
		}
	}
}

/**
 * Get the latest hierarchical taxonomy children of a post
 *
 * @author  Mauricio Gelves - <yo@maugelves.com>
 * @param	$post_id  int Post ID
 * @return  array
 */
function get_last_children_taxonomy_in_post( $post_id, $taxonomy = 'location'){

    // Parameter check
    if( empty($post_id) || !is_numeric($post_id) ) return false;
    
    //Get all terms associated with post in a hierarchical taxonomy (default 'category')
    $terms = get_the_terms( $post_id, $taxonomy );

    //Get an array of their IDs
    $term_ids = wp_list_pluck( $terms,'term_id' );

    //Get array of parents - 0 is not a parent
    $parents = array_filter( wp_list_pluck( $terms, 'parent' ) );

    //Get array of IDs of terms which are not parents.
    $term_ids_last_child = array_diff( $term_ids,  $parents );

    //Get corresponding term objects
    $terms_not_parents = array_intersect_key( $terms,  $term_ids_last_child );

    return $terms_not_parents;

}

/**
 * Get latest children category/ies of a post
 *
 * @author  Mauricio Gelves <yo@maugelves.com>
 * @param	$post_id  int Post ID
 * @return  array
 */
function get_last_children_categories($post_id){
    
    // Parameter check
    if( empty($post_id) || !is_numeric($post_id) ) return false;
    
    return get_last_children_taxonomy_in_post( $post_id );
    
}

// Change tax title if location is Guide

add_filter( 'pre_get_document_title', function ( $title ) {
	if( (is_tax('location')) && (in_category('guides')) ) {
        $taxonomy = get_queried_object();
        $term = $taxonomy->name;
        $term_id = $taxonomy->term_id;
        
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides', 
            'tax_query' => array(
                array(
                'taxonomy' => 'location',
                'field' => 'term_id',
                'terms' => $term_id,
                 )
              )
        );
        $check_guide = new WP_query ( $args );
        if($check_guide->have_posts()) { 
            while ( $check_guide->have_posts() ) {
                $check_guide->the_post();

                $post_id = get_the_ID();
                $terms =  wp_get_object_terms($post_id,'location');
                $deep_terms = get_deep_child_terms($terms);

                foreach ($deep_terms as $deep_term) {
                    
                    $post_term_id = $deep_term->term_id;

                    if ($post_term_id == $term_id ) {
                         $title = get_the_title() . ' - Путеводитель - '. get_bloginfo( 'name' ); 
                    }
                }
            }
        }
    }
    
	return $title;
}, 999, 1 );


// Redirect Guide post to Location Term

function redirect_guide_to_location() {
    
    if( (is_single()) && (in_category( 'guides' ))) {

        $post_id = get_the_ID();
        $terms =  wp_get_object_terms($post_id,'location'); 
        
        if($terms) {
        $deep_terms = get_deep_child_terms($terms);
        $count_terms = count($deep_terms);
        
        if  ($count_terms > 1) { echo 'У путеводителя несколько локаций'; exit; }
        
            foreach ($deep_terms as $deep_term) {
                $check_type = get_term_meta( $deep_term->term_id, 'location-type', true );
                if ($check_type == 'region') { echo 'У путеводителя выбран только Регион'; exit; }
                $term_url = get_term_link($deep_term->term_id,'location');
            }

        }
        else { echo 'У путеводителя не выбрана локация'; exit; }
        
        if($term_url) {  // check if we have a URL to redirect
            wp_redirect($term_url);
        }
        
        exit;
    }
} 

if (current_user_can('editor') || current_user_can('administrator')) { } else {
add_action('template_redirect', 'redirect_guide_to_location'); }

// Post Location breadcrumbs
function post_location_breadcrumbs() {
    
        $post_id = get_the_ID();
        $terms =  wp_get_object_terms($post_id,'location'); 
        
        if(!empty($terms)) {
            $deep_terms = get_deep_child_terms($terms);
            
            echo '<div class="breadcrumbs__wrapper">';
            foreach ($deep_terms as $deep_term) {
                
                $term_id = $deep_term->term_id;
                $check_type = get_term_meta( $term_id, 'location-type', true );
                
                if ($check_type == 'country') {
                    
                        $country_flag = get_term_meta( $term_id, 'flag-url', true );
                        $country_title = $deep_term->name;
                        $country_url = get_term_link($term_id);   
                    
                        if(!empty($country_flag)) {
                            echo '<img src="' . esc_attr( $country_flag ). '">';
                        }

                        echo '<div class="breadcrumbs__item">';
                        echo '<a href="' . $country_url .'">';
                        echo $country_title ;
                        echo '</a>' . "\n";
                        echo '</div>' . "\n";
                }
                else if ($check_type == 'city') {
                        $country_id = wp_get_term_taxonomy_parent_id($term_id, 'location');

                        $country_term = get_term( $country_id, 'location');
                        $country_flag = get_term_meta( $country_id, 'flag-url', true );

                        $country_title = $country_term->name;
                        $country_url = get_term_link($country_term->term_id);  
                        $city_title = $deep_term->name;
                    
                        $city_url = get_term_link($term_id);     

                        if(!empty($country_flag)) {
                            echo '<img src="' . esc_attr( $country_flag ). '">';
                        }

                        echo '<div class="breadcrumbs__item">';
                        echo '<a href="' . $country_url .'">';
                        echo $country_title ;
                        echo '</a>';
                        echo '<span> / </span>';
                        echo '<a href="' . $city_url .'">';
                        echo $city_title ;
                        echo '</a>';
                        echo '</div>';
                }
            }
        }
        echo '</div>';
}

// Return All Guide IDs

function get_guide_terms() {
    if( $item->object === 'category'  ) {
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides', 

            );
            $posts = new WP_query ( $args ); 
            $post_ids = wp_list_pluck( $posts->posts, 'ID' );

            foreach ($post_ids as $post_id)
            {
                $terms =  wp_get_object_terms($post_id,'location'); 
                $check_guide_deep_terms = get_the_terms( $post_id, 'location' );
            
                foreach($check_guide_deep_terms as $check_guide_deep_term) {
                    
                    $deep_terms = get_categories( array ('taxonomy' => 'location', 'parent' => $check_guide_deep_term->term_id ));

                    if ( count($deep_terms) == 0 ) {
                        $term = $check_guide_deep_term;
                }
                
                $term_id = $term->term_id;

                $check_type = get_term_meta( $term->term_id, 'location-type', true );
            }
            }
        
    }
    return $term;
}

// Return last taxonomy term

function get_deep_child_terms( $taxonomy ) {
	$taxonomy_tmp = $taxonomy;
	foreach ( $taxonomy_tmp as $child_term ) {
        $parent_term = $child_term->parent;
		foreach ( $taxonomy_tmp as $key => $parent_term ) {
			if ( isset( $taxonomy[ $key ] ) ) {
				if ( cat_is_ancestor_of( $parent_term, $child_term ) ) {
					unset( $taxonomy[ $key ] );
				}
			}
		}
	}

	return $taxonomy;
}

// Show modified metabox
    
add_action( 'add_meta_boxes', 'add_modified_box' );
 
    function add_modified_box( $post ) {
        add_meta_box(
            'Meta Box', // ID, should be a string.
            'Отображать дату обновления', // Meta Box Title.
            'modified_meta_box', // Your call back function, this is where your form field will go.
            'post', // The post type you want this to show up on, can be post, page, or custom post type.
            'side', // The placement of your meta box, can be normal or side.
            'core' // The priority in which this will be displayed.
        );
}
 
  function modified_meta_box($post) {
    wp_nonce_field( 'my_awesome_nonce', 'awesome_nonce' );    
    $checkboxMeta = get_post_meta( $post->ID );
    ?>
 
    <input type="checkbox" name="show_modified" id="show_modified" value="yes" <?php if ( isset ( $checkboxMeta['show_modified'] ) ) checked( $checkboxMeta['show_modified'][0], 'yes' ); ?> />Отображать<br />
 
<?php }


add_action( 'save_post', 'save_modified_checkboxes' );
    function save_modified_checkboxes( $post_id ) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
            return;
        if ( ( isset ( $_POST['my_awesome_nonce'] ) ) && ( ! wp_verify_nonce( $_POST['my_awesome_nonce'], plugin_basename( __FILE__ ) ) ) )
            return;
        if ( ( isset ( $_POST['post_type'] ) ) && ( 'page' == $_POST['post_type'] )  ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return;
            }    
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return;
            }
        }
 
        //saves value
        if( isset( $_POST[ 'show_modified' ] ) ) {
            // обычное поведение, прописываем дату измения
            update_post_meta( $post_id, 'show_modified', 'yes' );
        } else {
           // сброс даты обновления поста на дату создания поста         
            $post_id = get_the_ID();
            $datetime = get_the_time ('Y-m-d H:i:s', $post_id);  
            $gmt_timestamp = get_post_time('Y-m-d H:i:s', true);
            
            global $wpdb;

            $wpdb->query( "UPDATE `$wpdb->posts` SET 
                                    `post_modified` = '".$datetime."',
                                    `post_modified_gmt` = '".$gmt_timestamp."'
                                    WHERE `ID` = '".$post_id."'" );
            
            update_post_meta( $post_id, 'show_modified', 'no' );
        } 
}

/*
// Register the image column
function image_column_register( $columns ) {
    $columns['image_list'] = __( 'Размер картинки', 'my-plugin' );
  
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'image_column_register' );
 
// Display the image column content
function image_column_display( $column_name, $post_id ) {
    if ( 'image_list' != $column_name )
        return;
    $post_id = get_post_thumbnail_id();
    $image = wp_get_attachment_metadata($post_id);
    
    if ($image['width'] < '500') { echo '<span style="color:red;font-weight:700;">'.$image['width'].'</span>'; }
    else { echo $image['width']; }
    
}
add_action( 'manage_posts_custom_column', 'image_column_display', 10, 2 );
 
// Register the image column as sortable
function image_column_register_sortable( $columns ) {
    $columns['image_list'] = 'modified_list';
  
    return $columns;
}
add_filter( 'manage_edit-post_sortable_columns', 'image_column_register_sortable' );

*/
// Register the modified column
function modified_column_register( $columns ) {
    $columns['modified_list'] = __( 'Изменено', 'my-plugin' );
  
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'modified_column_register' );
 
// Display the modified column content
function modified_column_display( $column_name, $post_id ) {
    if ( 'modified_list' != $column_name )
        return;
    echo 'Изменено<br><strong>';
    echo the_modified_date();
    echo ' в ';
    echo the_modified_time();
    echo '</strong>';
}
add_action( 'manage_posts_custom_column', 'modified_column_display', 10, 2 );
 
// Register the column as sortable
function modified_column_register_sortable( $columns ) {
    $columns['modified_list'] = 'modified_list';
  
    return $columns;
}
add_filter( 'manage_edit-post_sortable_columns', 'modified_column_register_sortable' );


// Register the users column
function users_column_register( $columns ) {
    $columns['post_users'] = __( 'Псевдоавторы', 'my-plugin' );
  
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'users_column_register' );
 
// Display the users column content
function users_column_display( $column_name, $post_id ) {
    if ( 'post_users' != $column_name )
        return;
   $post_authors = wp_get_post_terms(get_the_ID(), 'users', array('fields' => 'ids')); 
    foreach ($post_authors as $post_author) {
        
        $user = get_term($post_author, 'users');
        $display_users[] = '<a href="edit.php?users='.$user->slug.'">'.$user->name.'</a>';
    }
    if (!empty($display_users)) { $display_users = implode(', ', $display_users); echo $display_users; }
    
}
add_action( 'manage_posts_custom_column', 'users_column_display', 10, 2 );


// Guide IDs

function return_guide_ids() {
        $guide_ids = [];
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides', 
            );
    
            $posts = new WP_query ( $args ); 
            $post_ids = wp_list_pluck( $posts->posts, 'ID' );
        
            foreach ($post_ids as $post_id) {
                $terms =  wp_get_object_terms($post_id,'location'); 
                $deep_terms = get_deep_child_terms($terms);
                
                foreach ($deep_terms as $deep_term) {
                    $term_id = $deep_term->term_id;
                    $guide_ids[] .= $deep_term->term_id;
                }
            }
        
  return $guide_ids;
}

function return_guide_names() {
        $guide_names = [];
        $args = array(
            'post_type'   => 'post',
            'category_name' => 'guides', 
            );
    
            $posts = new WP_query ( $args ); 
            $post_ids = wp_list_pluck( $posts->posts, 'ID' );
        
            foreach ($post_ids as $post_id) {
                $terms =  wp_get_object_terms($post_id,'location'); 
                $deep_terms = get_deep_child_terms($terms);
                
                foreach ($deep_terms as $deep_term) {
                    $term_id = $deep_term->term_id;
                    $guide_names[] .= $deep_term->name;
                }
            }
        
  return $guide_names;
}

function show_guide_menu($post_id) {
  
$guide_menu = get_field('guide_menu', $post_id);

if( $guide_menu[0] == '1' ) {  
    
    $menu .= '<div class="guide-menu__wrapper">';
    $menu .= '<div class="guide-menu__list">';
   

    if( have_rows('guide_menu__menu_item', $post_id) ):

        $menu .= '<ol class="ordered-list">';
    
        while( have_rows('guide_menu__menu_item', $post_id) ) : the_row();

            // Get parent value.
            $guide_menu__menu_item_title = get_sub_field('guide_menu__menu_item_title', $post_id);
            $guide_menu__menu_item_link = get_sub_field('guide_menu__menu_item_link', $post_id);
    
            $menu .= '<li><a href="#'. $guide_menu__menu_item_link .'">'. $guide_menu__menu_item_title .'</a></li>';
    
            // Loop over sub repeater rows.
            $guide_menu__submenu_item_check = get_sub_field('guide_menu__submenu_item_check', $post_id);
            
            if( (have_rows('guide_menu__submenu_item')) && ($guide_menu__submenu_item_check[0] == 1)):
                $menu .= '<ol class="ordered-sub-list">';
                while( have_rows('guide_menu__submenu_item') ) : the_row();

                    $guide_menu__submenu_item_title = get_sub_field('guide_menu__submenu_item_title', $post_id);
                    $guide_menu__submenu_item_link = get_sub_field('guide_menu__submenu_item_link', $post_id);
                    $menu .= '<li><a href="#'. $guide_menu__submenu_item_link .'">'. $guide_menu__submenu_item_title .'</a></li>';
    
                endwhile;
                $menu .= '</ol>';
            endif;
        endwhile;
    
        $menu .= '</ol>';
        $menu .= '</div>';
        $menu .= '</div>';

    endif;
    
    }
    
    return $menu;
}

// Insert guide menu to post

add_filter( 'the_content', 'insert_guide_menu' );
function insert_guide_menu( $content ) {
    global $post;
    
    $post_id = get_the_ID();
	$ad_code = show_guide_menu($post_id);
    $guide_position = get_field('guide_menu__position', $post_id);
    

	if( (is_tax( 'location' ) ) && (!empty($guide_position)) || ((in_category('guides')) && (!empty($guide_position)) )) {
		return insert_guide_menu_after_position( $ad_code, $guide_position, $content );
	}
return $content;
}
 
// Guide menu position

function insert_guide_menu_after_position( $insertion, $paragraph_id, $content ) {
	$closing_p = '</p>';
	$paragraphs = explode( $closing_p, $content );
	foreach ($paragraphs as $index => $paragraph) {

		if ( trim( $paragraph ) ) {
			$paragraphs[$index] .= $closing_p;
		}

		if ( $paragraph_id == $index + 1 ) {
			$paragraphs[$index] .= $insertion;
		}
	}
	
	return implode( '', $paragraphs );
}


function wp_get_attachment( $attachment_id ) {

    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

function gallery_attribute( $content, $id ) {
	// Restore title attribute
	$title = get_the_title( $id );
	return str_replace('<a', '<a data-fslightbox="gallery" title="' . esc_attr( $title ) . '" ', $content);
}
add_filter( 'wp_get_attachment_link', 'gallery_attribute', 10, 4 );

// Single images


function image_attribute( $content ) {
       
    

//$c = preg_replace(array("/<img[^>]+\>/i", "/<a[^>]*>(.*)<\/a>/iU",'%(\\[caption.*])(.*)(\\[/caption\\])%'), array('','','$2'), $content);
    
 //echo $c;    

       $pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
       $replace = '<a$1href=$2$3.$4$5 data-fslightbox="gallery" data-caption="">';
       $content = preg_replace( $pattern, $replace, $content );
       return $content;
}
add_filter( 'the_content', 'image_attribute' );




if (current_user_can( 'manage_options' )) {
// Redirect metabox
## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'redirect_add_custom_box');
function redirect_add_custom_box(){
	$screens = array( 'post', 'page' );
	add_meta_box( 'redirect_meta_box', 'Редирект', 'redirect_meta_box_callback', $screens, 'normal', 'low' );
}

// HTML код блока
function redirect_meta_box_callback( $post, $meta ){
	$screens = $meta['args'];

	// Используем nonce для верификации
	wp_nonce_field( plugin_basename(__FILE__), 'redirect_noncename' );

	// значение поля
	$value = get_post_meta( $post->ID, 'redirect_meta_key', 1 );

	// Поля формы для введения данных
	echo '<label for="redirect_new_field">' . __("Ссылка", 'redirect_textdomain' ) . '</label> ';
	echo '<input type="text" id="redirect_new_field" name="redirect_new_field" value="'. $value .'" size="25" />';
}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'redirect_save_postdata' );
function redirect_save_postdata( $post_id ) {
	// Убедимся что поле установлено.
	if ( ! isset( $_POST['redirect_new_field'] ) )
		return;

	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['redirect_noncename'], plugin_basename(__FILE__) ) )
		return;

	// если это автосохранение ничего не делаем
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	// проверяем права юзера
	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	$redirect_data = sanitize_text_field( $_POST['redirect_new_field'] );

	// Обновляем данные в базе данных.
	update_post_meta( $post_id, 'redirect_meta_key', $redirect_data );
}
}
// Top term

function get_top_term( $taxonomy, $post_id = 0 ) {
	if( isset($post_id->ID) ) $post_id = $post_id->ID;
	if( ! $post_id )          $post_id = get_the_ID();

	$terms = get_the_terms( $post_id, $taxonomy );

	if( ! $terms || is_wp_error($terms) ) return $terms;

	// только первый
	$term = array_shift( $terms );

	// найдем ТОП
	$parent_id = $term->parent;
	while( $parent_id ){
		$term = get_term_by( 'id', $parent_id, $term->taxonomy );
		$parent_id = $term->parent;
	}

	return $term;
}



// Add Lead metabox to search

add_filter( 'posts_join', 'cf_search_join' );
add_filter( 'posts_where', 'cf_search_where' );
add_filter( 'posts_distinct', 'cf_search_distinct' );

# Объединяет таблицы записей и таблиц метаданных.
function cf_search_join( $join ){
	global $wpdb;

	if( is_search() )
		$join .= " LEFT JOIN $wpdb->postmeta ON ID = $wpdb->postmeta.post_id ";

	return $join;
}

# Указывает по каким метаполям и какое значение искать в секции WHERE.
function cf_search_where( $where ){
	global $wpdb;

	if ( is_search() ) {
		$where = preg_replace(
			"/\(\s*$wpdb->posts.post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
			"($wpdb->posts.post_title LIKE $1) OR ($wpdb->postmeta.meta_value LIKE $1)", $where );
	}

	return $where;
}

# Предотвращает появление дубликатов в выборке.
function cf_search_distinct( $where ){
	return is_search() ? 'DISTINCT' : $where;
}




add_action( 'add_meta_boxes', 'add_lead_meta_box' );
 
    function add_lead_meta_box( $post ) {
        add_meta_box(
            'lead_meta_box', // ID, should be a string.
            'Лид', // Meta Box Title.
            'modified_lead_meta_box', // Your call back function, this is where your form field will go.
            'post', // The post type you want this to show up on, can be post, page, or custom post type.
            'my_custom_context', // The placement of your meta box, can be normal or side.
            'high' // The priority in which this will be displayed.
        );
}
 
  function modified_lead_meta_box($post) {
    wp_nonce_field( basename( __FILE__ ), 'textarea_nonce' );
    $textarea_stored_meta = get_post_meta( $post->ID );
    ?>
<p><label for="wpsites_textarea" class="screen-reader-text"></label></p>
<p>
		<textarea name="lead_meta_box_text" id="lead_meta_box_text" style="width: 100%;height: 50px;"><?php if ( isset ( $textarea_stored_meta['lead_meta_box'] ) ) echo $textarea_stored_meta['lead_meta_box'][0]; ?></textarea>
	</p>
<p><input type="checkbox" name="lead_meta_box_hr" id="lead_meta_box_hr" value="yes" <?php if ( isset ( $textarea_stored_meta['lead_meta_box_hr'] ) ) checked( $textarea_stored_meta['lead_meta_box_hr'][0], 'yes' ); ?> />Отображать ёлочки после лида<br /></p>
	<?php
}


function textarea_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'textarea_nonce'] ) && wp_verify_nonce( $_POST[ 'textarea_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
    // Checks for input and saves if needed
	if( isset( $_POST[ 'lead_meta_box_text' ] ) ) {
		update_post_meta( $post_id, 'lead_meta_box', $_POST[ 'lead_meta_box_text' ] );
	}
    if( isset( $_POST[ 'lead_meta_box_hr' ] ) ) {
        update_post_meta( $post_id, 'lead_meta_box_hr', 'yes' );
    } else {
        update_post_meta( $post_id, 'lead_meta_box_hr', 'no' );
    } 

}
add_action( 'save_post', 'textarea_meta_save');

add_action( 'edit_form_after_title', 'show_custom_meatbox' );
function show_custom_meatbox( $post ) {
	do_meta_boxes( null, 'my_custom_context', $post );
}

function metabox_not_sortable($classes) {
    $classes[] = 'not-sortable';
    return $classes;
}
add_filter('postbox_classes_post_lead_meta_box', 'metabox_not_sortable');


add_action('admin_print_footer_scripts','my_admin_print_footer_scripts',99);
function my_admin_print_footer_scripts()
{
    ?><script type="text/javascript">/* <![CDATA[ */
        jQuery(function($)
        {
            $(".meta-box-sortables")
                // define the cancel option of sortable to ignore sortable element
                // for boxes with '.not-sortable' css class
                .sortable('option', 'cancel', '.not-sortable .hndle, :input, button')
                // and then refresh the instance
                .sortable('refresh');

        });
             
    /* ]]> */</script><?php
}

function return_lead($post_id) {
    $post_lead = get_post_meta($post_id, 'lead_meta_box');
    if (!empty($post_lead[0])) { $lead = $post_lead[0]; }
    return $lead;
}

// Plural Forms

function plural_form($number, $after) {
	$cases = array (2, 0, 1, 1, 1, 2);
	echo $after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
}

// Youtube responsive

add_filter('the_content', function($content) {
	return str_replace(array("<iframe", "</iframe>"), array('<div class="iframe-container"><iframe', "</iframe></div>"), $content);
});

add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
	if(strpos($html, 'youtube.com') !== false || strpos($html, 'youtu.be') !== false){
  		return '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
	} else {
	 return $html;
	}
}, 10, 4);


add_filter('embed_oembed_html', function($code) {
  return str_replace('<iframe', '<iframe class="embed-responsive-item" ', $code);
});



// YOAST SEO

// Yoast SEO disable primary cat
add_filter( 'wpseo_primary_term_taxonomies', '__return_empty_array' );

// Disable comments
add_filter( 'wpseo_debug_markers', '__return_false' );

// Disable article:published_time & article:modified_time

add_filter('wpseo_frontend_presenter_classes', function($filter) {
        return array_diff($filter, [
            'Yoast\WP\SEO\Presenters\Open_Graph\Article_Published_Time_Presenter',
            'Yoast\WP\SEO\Presenters\Open_Graph\Article_Modified_Time_Presenter',
        ]);
    }
);


add_filter( 'wpseo_opengraph_desc', 'filter_post_desc_yoast_meta' );
add_filter( 'wpseo_metadesc', 'filter_post_desc_yoast_meta' );
function filter_post_desc_yoast_meta( $filter ){
    
    $guide_ids = return_guide_ids();   
    
	if( (is_single()) || ((!empty($guide_ids)) && (is_tax('location', $guide_ids) ))){
        if (!empty( return_lead( get_the_ID() ) ) ) { 
            $filter = return_lead( get_the_ID() ); 
        } else { 
            $filter = wp_trim_words( get_the_content(), 55, '&hellip;' ); 
        }
    }

    if ((is_tax('location')) || (is_tag()) ) {
        
        $queried_object = get_queried_object();
        $term_id = $queried_object->term_id;
        
        $meta = get_option( 'wpseo_taxonomy_meta' );
        $yoast_meta = $meta['location'][$term_id]['wpseo_desc'];

        if ($yoast_meta) { //check if the variable(with meta value) isn't empty
            $filter = $yoast_meta;
        }
        else {
            $filter = get_bloginfo('description');
        }
    }
    
	return $filter;
}

add_filter( 'wpseo_next_rel_link', 'wpseo_links_remove' );
add_filter( 'wpseo_prev_rel_link', 'wpseo_links_remove' );
function wpseo_links_remove( $filter ){
    return false;
	return $filter;
}

add_filter( 'wpseo_opengraph_image', 'change_img_yoast_meta' );
function change_img_yoast_meta( $filter ){
    
    if (is_tax('location'))  {
        $term_id = get_queried_object()->term_id; 
        $image_id = get_term_meta( $term_id, '_thumbnail_id', 1 ); 
        $image_url = wp_get_attachment_image_url( $image_id, 'large' );
    
        $filter = $image_url;
    }
    $guide_ids = return_guide_ids();    
        
    if ((!empty($guide_ids)) && (is_tax('location', $guide_ids) )) {
        $filter = get_the_post_thumbnail_url();
    }
        
    return $filter;
    
}

add_filter( 'wpseo_opengraph_image_size', 'override_images_size');

	function override_images_size() {
        if (is_tax('location'))  {
		return 'large';
	}
}

add_filter( 'wpseo_opengraph_desc', 'filter_guide_desc_yoast_meta' );
add_filter( 'wpseo_metadesc', 'filter_guide_desc_yoast_meta' );
function filter_guide_desc_yoast_meta( $filter ){
    
    $guide_ids = return_guide_ids();   
    
    if( (!empty($guide_ids)) && (is_tax('location', $guide_ids) )){
        if (!empty( return_lead( get_the_ID() ) ) ) { 
            $filter =  return_lead( get_the_ID() ); 
        } else { 
            $filter = wp_trim_words( get_the_content(), 55, '&hellip;' ); 
        }
    }
    return $filter;
}

add_filter( 'wpseo_opengraph_title', 'filter_guide_title_yoast_meta' );
function filter_guide_title_yoast_meta( $filter ){
    
    $guide_ids = return_guide_ids();   
    
    if( (!empty($guide_ids)) && (is_tax('location', $guide_ids) )){
        $filter = get_the_title() . ' - Путеводитель - ' . get_bloginfo('name');
    }
    return $filter;
}



function show_random_bg() {
    $images = array();
    
    $images[] = 'pattern1.svg';
    $images[] = 'pattern2.svg';
    $num = array_rand($images);
    return $images[$num];
}
