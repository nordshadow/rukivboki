<?php
/**
 * Template Name: Tag
 */
get_header();
?>
    <?php

    $tag = get_queried_object();
   
    $args = array(
        'post_type'   => 'post',
        'posts_per_page' => '6',
        'tag' => $tag->slug,
        'paged' => 1,
        );

    $tag_posts = new WP_query ( $args );

    ?>

    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header-front">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/tag.svg" width="30" height="30">
                <h2><?php single_tag_title(); ?></h2>
            </div>
        </div>
        <div class="container p-0">
                <div class="section-content fade">
                    <div id="posts" class="card-container" data-max-page="<?php echo $tag_posts->max_num_pages;?>" data-page-type="tag" data-cat="<?php echo $tag->slug;?>">
                        <?php if ( $tag_posts->have_posts() ) {
                        while ( $tag_posts->have_posts() ) {
                                $tag_posts->the_post(); ?>
                        <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
                            <a href="<?php the_permalink()?>">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                </div>
                            </a>
                        </div>
                        <? }
                        } else {
                            // No posts
                        }
                        wp_reset_postdata(); ?>
                    </div>
                    <?php if (  $tag_posts->max_num_pages > 1 ) { ?>
                        <div class="section-button">
                            <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
                            <div id="loader" style="display:none; margin:10px 0;"></div>
                        </div>
                    <? } ?>
                </div>
            </div>
    </section>

<?php 
get_footer(); 
?>
