<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google-site-verification" content="WssdD7rEAOlrDE_QKv24-QMFt7fG6bnPhCIW3kB4iWM" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <style>
        .post-content {
        background: url('<?php echo get_template_directory_uri() .'/assets/img/'. show_random_bg();?>') repeat #f8f7fc;
            }
    </style>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
<?php wp_body_open(); ?>
<?php if(is_front_page()) { echo '<div class="full-logo"><img src="' . get_template_directory_uri() .'/assets/img/logo-full.svg" alt="Руки-в-боки"></div>'; } ?>
<header id="site-header" class="header__wrapper <?php if ( is_admin_bar_showing() )  { echo 'ws-bar-top '; } if(is_front_page()) { echo 'logo-offset';}?>">
    <div class="header__menu-wrapper">
        <div class="header__logo <?php if(is_front_page()) { echo 'header__logo-front'; } else { echo 'header__logo-post'; }?>"><a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" width="150" height="35"></a></div>
        <div class="header__nav-wrapper">
            <div class="header__nav">
                <div class="header__nav-mobile" id="nav-menu">
                    <div id="menu-close" class="close-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/close.svg" width="18" height="18" alt="Закрыть"></div>
                    <div class="full-logo-menu"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-full.svg" alt="Руки-в-боки"></div>
                    <? 
                    wp_nav_menu( [
                                  'theme_location' => 'header_menu',
                                  'depth'          => 0,
                                  'container'      => false,
                                  'fallback_cb'    => false,
                                  'echo'           => true,
                                  'walker'         => new BEM_Walker_Nav_Menu(),
                                  'bem_block'      => 'header-nav',
                                  'items_wrap'     => '
                                      <ul class="header-nav__list">%3$s</ul>
                                  ',

                                ] );
                    ?>
                    <div id="search-modal" class="header__search">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 57 57" fill="#fff"><path class="st0" d="M55.1,51.9L41.6,37.8C45.1,33.6,47,28.4,47,23C47,10.3,36.7,0,24,0S1,10.3,1,23s10.3,23,23,23
	c4.8,0,9.3-1.4,13.2-4.2L50.8,56c0.6,0.6,1.3,0.9,2.2,0.9c0.8,0,1.5-0.3,2.1-0.8C56.3,55,56.3,53.1,55.1,51.9z M24,6
	c9.4,0,17,7.6,17,17s-7.6,17-17,17S7,32.4,7,23S14.6,6,24,6z"/></svg>
                        <span>Поиск</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu-toggle" class="toggle-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/bars.svg" width="18" height="18" alt="Меню"></div>
    </div>
</header>
    
<?php $guide_ids = return_guide_ids(); ?>
<main class="<?php if(is_single()) { echo 'post-content'; } else if(is_page('travel')) { echo 'page-content'; } else if(is_front_page()) { echo 'page-front'; } else if ((!empty($guide_ids)) && (is_tax('location', $guide_ids) )) { echo 'post-content'; } else if ((is_page('blog'))) { echo 'post-content'; } else if(is_page()) { echo 'post-content'; } else if ((is_404()) || (is_archive()) || (is_search()) ) { echo 'page-content'; } ?>">