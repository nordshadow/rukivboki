﻿<?php
/**
 * Template Name: Search
 */
get_header();

?>

<?php

    // Latest Posts

    /*$guide_cat = get_category_by_slug('guides');
    $innovation_cat = get_category_by_slug('innovation');
    $notes_cat = get_category_by_slug('advice');
    $recipe_cat = get_category_by_slug('recipe');

    $cat_not_in = array ($guide_cat->term_id, $innovation_cat->term_id, $notes_cat->term_id, $recipe_cat->term_id);
    $cat_not_in = implode(', ', $cat_not_in);
*/
    $search_text = get_search_query();

    $args = array(
        'post_type'   => 'post',
        /*'category__not_in' => array($cat_not_in), */
        's' => $search_text,
        'orderby' => 'modified',
        'posts_per_page' => '6',
        'paged' => 1,
        );

    $search_posts = new WP_query ( $args );

    ?>

    <section>
        <div class="section-wrapper container-fluid fade">
            <div class="section-header">
            <h1>Результаты поиска</h1>
            </div>
            <div class="container p-0">
                <div class="section-content fade">
                    <div class="search-results__wrapper">
                    <div class="search-results__text">
                    Вы искали: <span><?php echo get_search_query(); ?></span>
                    </div>
                    <div class="search-results__count"><?php plural_form($wp_query->found_posts,/* варианты написания для количества 1, 2 и 5 */ array('Найдена','Найдено','Найдено')); ?>
                    <?php echo ': '.$wp_query->found_posts.' '; plural_form($wp_query->found_posts,/* варианты написания для количества 1, 2 и 5 */ array('публикация','публикации','публикаций')); ?>
                    </div>
                    </div>  
                    
                    <form name="search" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                         <div class="search-box__wrapper">  
                            <input type="text" id="s" class="search-box__input" name="s" value="<?php echo get_search_query(); ?>" placeholder="Что ищем?" autocomplete="off" required="required" />  
                            <button class="search-box__submit" onclick="searchform.submit()"><span>Искать</span>
                            </button>
                        </div>
                    </form>
                    
                    <div id="posts" class="card-container" data-max-page="<?php echo $search_posts->max_num_pages;?>" data-page-type="search" data-search-text="<?php echo $search_text; ?>">
                        <?php if ( $search_posts->have_posts() ) {
                        while ( $search_posts->have_posts() ) {
                                $search_posts->the_post(); ?>
                        <div class="card-wrapper-main card-wrapper-page card-post__wrapper fade">
                            <a href="<?php the_permalink()?>">
                                <div class="card-post__wrapper-inner">
                                    <div class="card-post__img" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID() , 'medium_large'); ?>') no-repeat center center / cover"></div>
                                    <div class="card-post__title"><?php the_title();?></div>
                                    <div class="card-post__excerpt"><?php if (!empty( return_lead( get_the_ID() ) ) ) { echo return_lead( get_the_ID() ); } else { echo wp_trim_words( get_the_content(), 55, '&hellip;' ); } ?></div>
                                </div>
                            </a>
                        </div>
                        <? }
                        } else {
                            // No posts
                        }
                        wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php 
    if ( $search_posts->max_num_pages > 1 ) { ?>
        <div class="section-button">
            <div class="loadmore button-show-all"><img src="<?php echo get_template_directory_uri();?>/assets/icons/loadmore.svg" width="15" height="15"><span>Загрузить ещё</span></div>
            <div id="loader" style="display:none; margin:10px 0;"></div>
        </div>

    <?php } 
?>

<?php
    wp_reset_postdata(); ?>

<?php 
get_footer(); 
?>